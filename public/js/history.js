
$(function() {
	var $body = $('body');

	$body.on('change', '.js-history-change-quantity', function() {
		var $this = $(this),
			url = $this.data('url'),
			quantity = parseInt($this.val());

		if (!quantity || quantity < 0) {
			$this.val($this.data('quantity'));
			alert('Only positive integers allowed');
			return;
		}

		$.ajax({
			url: url,
			data: {
				productId: $this.closest('tr').data('product-id'),
				historyId: $this.closest('form').data('history-id'),
				quantity: quantity
			},
			success: function () {
				location.reload();
			}
		});
	});
});
