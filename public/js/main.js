$(function () {
	var $body = $('body');

	/**
	 * left menu toggle
	 */
	$body.on('click', '.js-menu-toggle', function () {
		$('.js-nav-bar').toggleClass('nav-bar_opened');
		return false;
	});

	/**
	 * check/uncheck all checkboxes
	 */
	$body.on('change', '.js-cart-select-all', function () {
		var $this = $(this);
		$this.closest('table').find('.js-cart-select').prop('checked', $this.is(':checked'));
	});

	/**
	 * user edit table actions
	 */
	$('.js-edit-table')
		.on('click', '[data-edit]', function () {
			var $this = $(this),
				$readRow = $this.closest('[data-read-row]'),
				$editRow = $readRow.next();

			$readRow.hide();
			$editRow.removeClass('hidden');
			return false;
		})
		.on('click', '[data-accept], [data-cancel]', function () {
			var $this = $(this),
				$editRow = $this.closest('[data-edit-row]'),
				$readRow = $editRow.prev();

			if ($this.is('[data-accept]')) {
				var $formWrapper = $this.closest('[data-serialize-elements]');
				var data = {};
				$formWrapper.find(':input').each(function() {
					if (this.name) {
						data[this.name] = this.value;
					}
				});
				$.post($formWrapper.data('url'), data, function(data) {
					if (parseInt(data)) {
						location.reload();
					}
				});
			}

			$editRow.addClass('hidden');
			$readRow.show();
			return false;
		})
		.on('click', '[data-delete]', function () {
			var $this = $(this),
				url = $this.data('delete-url'),
				$confirmation = $('#confirmation-modal');

			$confirmation.modal();

			$body.one('modal.yes', function() {
				$.get(url, function (response) {
					if (parseInt(response)) {
						$this.closest('tr').fadeOut(function () {
							var $this = $(this);

							if ($this.is('[data-read-row]')) { // this is only for user admin panel
								$this.next().remove();
							}
							$this.remove();
						});
					}
					$confirmation.modal('hide');
				});
			});
			return false;
		});

	// -- Modal event triggering
	$body.on('click', '.js-modal-yes', function() {
		$body.trigger('modal.yes');
		return false;
	});
	$body.on('click', '.js-modal-no', function () {
		$body.trigger('modal.no');
		return false;
	});
	$body.on('modal.no', function() {
		$('#confirmation-modal').modal('hide');
	});
	// -- -- -- --

	// -- Init select picker
	$('.js-select-picker').selectpicker({
		liveSearch: true,
		width: '100%'
	});

	var timeout = 0;
	var $pickerByQuery = $('.js-picker-get-options-by-query').on('keyup', '.bs-searchbox input', function() {
		// get option for select picker by ajax
		var $this = $(this),
			$targetSelect = $this.closest('.js-select-picker').siblings('.js-select-picker'),
			url = $targetSelect.data('url');

		clearTimeout(timeout);
		timeout = setTimeout(function() {
			$.get(url, {q: $this.val()}, function(data) {
				$targetSelect.html(data).selectpicker('refresh');
			});
		}, 500);
	});

	$('select.js-submit-on-change').on('change', function() {
		var $that = $(this);
		if ($that.val() !== '') {
			$('[name="clear"], [name="send"]').remove(); // to prevent clearing
			$that.closest('form').submit();
		} else {
			$('[name=send]').prop('disabled', true)
		}
	});
	// -- -- -- --

	// -- Init date picker
	$('.js-datepicker').datepicker({
		format: 'mm/dd/yyyy',
		autoclose: true
	}).each(function() {
		var $this = $(this);
		$this.next().on('click', function() {
			$this.datepicker('show');
		});
	});
	// -- -- -- --

	$('.js-change-picker-url').on('change', function() {
		var $this = $(this);
		switch ($this.val()) {
			case 'customer':
				$pickerByQuery.data('url', $pickerByQuery.data('get-customers-url'));
				break;
			case 'contractor':
				$pickerByQuery.data('url', $pickerByQuery.data('get-contractors-url'));
				break;
		}
	}).trigger('change');

	// -- Statistic dropdowns for report type
	var softToggleShow = function($element, show) {
		if (typeof show == 'undefined') {
			show = true;
		}
		$element.css('visibility', show? 'visible': 'hidden');
	};
	var $button = $('.js-create-report-button'),
		$changeTypeSelect = $('.js-statistics-change-type'),
		$membersSelect = $('.js-statistics-type-members');

	softToggleShow($button, false);
	$changeTypeSelect.on('change', function () {
		var $this = $(this),
			type = $this.val();

		if (!type) {
			$membersSelect.html('');
			softToggleShow($button, false);
			return;
		}
		
		// -- Fill members dropdown
		var url = null;
		switch (type) {
			case 'customer':
				url = $this.data('get-customers-url');
				break;
			case 'contractor':
				url = $this.data('get-contractors-url');
				break;
		}
		if (url !== null) {
			$.get(url, function (html) {
				$membersSelect.html(html).trigger('change');
			});
		}
		// -- -- -- --
	}).trigger('change');

	$membersSelect.on('change', function () {
		var $this = $(this),
			value = $this.find('option:selected').text();

		$.get($changeTypeSelect.data('url'), {
			type: $changeTypeSelect.val(),
			customer: $changeTypeSelect.data('customer'),
			contractor: $changeTypeSelect.data('contractor'),
			member: value
		});
		softToggleShow($button, !!value);
	});
	// -- -- -- --

	$body.on('change', '.js-disable-on-change', function () {
		var $this = $(this),
			target = $this.data('target');
		$(target).prop('disabled', !$this.val());
	});
});