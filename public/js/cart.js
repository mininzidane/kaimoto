var Cart = function () {
	this.loaderId = 'ajax-loader';
};
Cart.prototype.buildPopup = function () {
	$.get('/ajax/get-cart/', function (data) {
		var cartSummarySelector = '.js-cart-summary',
			$cartSummary = $(cartSummarySelector),
			isHidden = $cartSummary.find('.cart-summary').is(':hidden');
		$cartSummary.remove();
		$('body').append(data);
		if (isHidden) {
			$(cartSummarySelector).find('.cart-summary').hide();
		}
	});
	return this;
};
Cart.prototype.showLoader = function () {
	var $loader = $('#' + this.loaderId).show();

	if ($loader.length == 0) {
		$('<div id="' + this.loaderId + '"><ul class="spinner"><li></li><li></li><li></li><li></li></ul></div>')
			.appendTo($('body'));
	}
	return this;
};
Cart.prototype.hideLoader = function () {
	$('#' + this.loaderId).hide();
	return this;
};
Cart.prototype.removeHighlightFromProducts = function (productIds) {
	$.each(productIds, function (i, item) {
		$('.js-product[data-product-id=' + item + ']').removeClass('product_checked');
	});
	return this;
};
Cart.prototype.updateCounter = function () {
	var $counter = $('.js-cart-counter'),
		url = $counter.data('url');

	$.get(url, function (data) {
		$counter.html(data);
	});
	return this;
};

$(function () {
	var $body = $('body'),
		cart = new Cart();

	cart.buildPopup();

	$('.js-products')
		.on('click', '.js-product', function () {
			var $this = $(this),
				url = $this.data('add-to-cart-url'),
				productId = $this.data('product-id');

			cart.showLoader();
			$.ajax({
				url: url,
				data: {productId: productId},
				success: function () {
					$this.addClass('product_checked');
					cart.buildPopup().updateCounter();
				},
				complete: function () {
					cart.hideLoader();
				}
			});
		});

	$body.on('change', '.js-change-quantity', function () {
		var $this = $(this),
			url = $this.data('url'),
			quantity = parseInt($this.val());

		if (!quantity || quantity < 0) {
			$this.val($this.data('quantity'));
			alert('Only positive integers allowed');
			return;
		}

		cart.showLoader();
		$.ajax({
			url: url,
			data: {productId: $this.closest('tr').data('product-id'), quantity: quantity},
			success: function () {
				cart.buildPopup();
			},
			complete: function () {
				cart.hideLoader();
			}
		});
	});

	$body.on('click', '.js-remove-products', function () {
		var $this = $(this),
			productIdsParams = [],
			productIds = [];

		$('.js-cart-select').filter(':checked').each(function () {
			var productId = $(this).closest('tr').data('product-id');
			productIdsParams.push('productId[]=' + productId);
			productIds.push(productId);
		});

		cart.showLoader();
		$.ajax({
			url: $this.data('url') + '?' + productIdsParams.join('&'),
			success: function () {
				cart.buildPopup()
					.updateCounter()
					.removeHighlightFromProducts(productIds);
			},
			complete: function () {
				cart.hideLoader();
			}
		});
		return false;
	});

	$body.on('click', '.js-cart-save-history', function () {
		var $this = $(this),
			productIds = [];

		$('.js-cart-select').each(function () {
			var productId = $(this).closest('tr').data('product-id');
			productIds.push(productId);
		});

		cart.showLoader();
		$.ajax({
			url: $this.data('url'),
			success: function () {
				cart
					.buildPopup()
					.updateCounter()
					.removeHighlightFromProducts(productIds);
			},
			complete: function () {
				cart.hideLoader();
			}
		});
		return false;
	});

	$body.on('click', '.js-cart-toggle', function () {
		$(this).closest('.js-cart-summary').find('.cart-summary').slideToggle();
	});

	/**
	 * Alert when try to click send or save with empty cart
	 */
	$body.on('click', '.js-empty-cart', function() {
		alert('商品を選んでください');
		return false;
	});
});