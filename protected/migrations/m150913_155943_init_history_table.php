<?php

use yii\db\Schema;

class m150913_155943_init_history_table extends \app\cli\Migration {

	public function safeUp() {
		$this->createTable('history', [
			'id'      => 'INT(5) unsigned NOT NULL AUTO_INCREMENT',
			'cart'    => 'TEXT NOT NULL',
			'userId'  => 'INT(5) UNSIGNED NOT NULL',
			'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);
	}

	public function safeDown() {
		$this->dropTable('history');
	}
}
