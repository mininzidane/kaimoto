<?php

use \app\models\Product;

class m151102_103022_add_product_purchase_column extends \app\cli\Migration {

	public function safeUp() {
		$this->addColumn(Product::tableName(), 'purchasePrice', 'INT(5) UNSIGNED NULL DEFAULT NULL AFTER price');
	}

	public function safeDown() {
		$this->dropColumn(Product::tableName(), 'purchasePrice');
	}
}
