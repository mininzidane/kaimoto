<?php

use \app\models\History;

class m151008_162556_add_field_date_modified_to_history extends \app\cli\Migration {

	public function safeUp() {
		$this->addColumn(History::tableName(), 'modified', 'VARCHAR(32) NULL DEFAULT NULL');
	}

	public function safeDown() {
		$this->dropColumn(History::tableName(), 'modified');
	}
}
