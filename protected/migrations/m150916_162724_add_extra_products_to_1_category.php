<?php

use yii\db\Schema;

class m150916_162724_add_extra_products_to_1_category extends \app\cli\Migration {

	public function safeUp() {
		$products = [
			['Milk', 'milk.jpg'],
			['Chips', 'chips.jpg'],
			['Snack', 'snack.jpg'],
			['Oil', 'oil.jpg'],
			['Eggs', 'eggs.jpg'],
			['Chocolate', 'chocolate.jpg'],
		];
		foreach ($products as $product) {
			$this->insert('product', [
				'title'      => $product[0],
				'code'       => uniqid(),
				'price'      => rand(100, 500),
				'picture'    => $product[1],
				'categoryId' => 1,
			]);
		}
	}

	public function safeDown() {

	}
}
