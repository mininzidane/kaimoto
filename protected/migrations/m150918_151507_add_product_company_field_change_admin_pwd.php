<?php

use app\models\User;
use app\models\Product;

class m150918_151507_add_product_company_field_change_admin_pwd extends \app\cli\Migration {

	public function safeUp() {
		// change admin pwd to 1111
		/** @var User $user */
		$user = User::find()->one();
		$user->password = $user->getHashedPassword('1111');
		$user->save(false, ['password']);

		$this->addColumn(Product::tableName(), 'company', 'VARCHAR(255) NULL DEFAULT NULL AFTER picture');

		/** @var Product[] $products */
//		$products = Product::find()->all();
//		$companies = ['A-company', 'B-company', 'C-company'];
//		foreach ($products as $product) {
//			$product->company = $companies[array_rand($companies)];
//			$product->save(false, ['company']);
//		}

	}

	public function safeDown() {
		$this->dropColumn(Product::tableName(), 'company');

		/** @var User $user */
		$user = User::find()->one();
		$user->password = $user->getHashedPassword('qwerty123456');
		$user->save(false, ['password']);
	}
}
