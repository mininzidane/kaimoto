<?php

use \app\models\Contractor;
use \app\models\Customer;

class m151109_115147_add_contractor_table_add_customer_table extends \app\cli\Migration {

	public function safeUp() {
		$this->createTable(Contractor::tableName(), [
			'id'          => 'INT(5) UNSIGNED NOT NULL AUTO_INCREMENT',
			'companyName' => 'VARCHAR(255) NOT NULL',
			'email'       => 'VARCHAR(255) NULL DEFAULT NULL',
			'created'     => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);
		$this->createTable(Customer::tableName(), [
			'id'      => 'INT(5) UNSIGNED NOT NULL AUTO_INCREMENT',
			'name'    => 'VARCHAR(255) NOT NULL',
			'address' => 'VARCHAR(255) NULL DEFAULT NULL',
			'number'  => 'VARCHAR(255) NULL DEFAULT NULL',
			'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);
	}

	public function safeDown() {
		$this->dropTable(Customer::tableName());
		$this->dropTable(Contractor::tableName());
	}
}
