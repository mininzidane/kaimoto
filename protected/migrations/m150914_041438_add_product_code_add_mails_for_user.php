<?php

use yii\db\Schema;
use app\models\Product;
use app\models\User;

class m150914_041438_add_product_code_add_mails_for_user extends \app\cli\Migration {

	public function safeUp() {
		$this->addColumn(Product::tableName(), 'code', 'VARCHAR(128) NULL DEFAULT NULL AFTER title');
		foreach (Product::find()->all() as $product) {
			$product->code = uniqid();
			$product->save(true, ['code']);
		}

		$this->addColumn(User::tableName(), 'senderEmail', 'VARCHAR(128) NULL DEFAULT NULL AFTER email');
		$this->addColumn(User::tableName(), 'recipientEmails', 'VARCHAR(255) NULL DEFAULT NULL AFTER senderEmail');
		foreach (User::find()->all() as $user) {
			$user->senderEmail = 'no-reply@kaimono.com';
			$user->recipientEmails = 'mininzidane@gmail.com, mininzidane@mail.ru';
			$user->save(true, ['senderEmail', 'recipientEmails']);
		}
	}

	public function safeDown() {
		$this->dropColumn(User::tableName(), 'recipientEmails');
		$this->dropColumn(User::tableName(), 'senderEmail');
		$this->dropColumn(Product::tableName(), 'code');
	}
}
