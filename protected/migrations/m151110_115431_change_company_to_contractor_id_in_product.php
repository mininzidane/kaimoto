<?php

use \app\models\Product;

class m151110_115431_change_company_to_contractor_id_in_product extends \app\cli\Migration {

	public function safeUp() {
		$this->alterColumn(Product::tableName(), 'company', 'INT(5) UNSIGNED NULL DEFAULT NULL');
		$this->renameColumn(Product::tableName(), 'company', 'contractorId');
	}

	public function safeDown() {
		$this->renameColumn(Product::tableName(), 'contractorId', 'company');
		$this->alterColumn(Product::tableName(), 'company', 'VARCHAR(255) NULL DEFAULT NULL');
	}
}
