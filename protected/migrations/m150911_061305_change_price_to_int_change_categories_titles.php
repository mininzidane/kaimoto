<?php

use yii\db\Schema;

class m150911_061305_change_price_to_int_change_categories_titles extends \app\cli\Migration {

	public function safeUp() {
		$this->alterColumn('product', 'price', 'INT(5) NULL DEFAULT NULL');
		$categories = [
			2 => 'Daily goods',
			3 => 'Other',
		];
		foreach ($categories as $id => $categoryTitle) {
			$this->update('category', ['title' => $categoryTitle], ['id' => $id]);
		}
	}

	public function safeDown() {
		$this->alterColumn('product', 'price', 'DECIMAL(6,2) NULL DEFAULT NULL');
	}
}
