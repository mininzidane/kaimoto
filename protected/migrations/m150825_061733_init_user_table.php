<?php

class m150825_061733_init_user_table extends app\cli\Migration {

	public function safeUp() {

		$this->createTable('user', [
			'id'          => 'int(5) unsigned NOT NULL AUTO_INCREMENT',
			'username'    => 'varchar(128) NOT NULL',
			'password'    => 'varchar(32) NOT NULL',
			'email'       => 'varchar(200) DEFAULT NULL',
			'role'        => 'varchar(64) NOT NULL',
			'description' => 'varchar(200) DEFAULT NULL',
			'created'     => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);

		$user = new \app\models\User();
		$this->insert('user', [
			'username' => 'admin',
			'password' => $user->getHashedPassword('qwerty123456'),
		]);
	}

	public function safeDown() {
		$this->dropTable('user');
	}
}
