<?php

use yii\db\Schema;

class m150916_143155_add_demo_products extends \app\cli\Migration {

	public function safeUp() {
		$this->truncateTable('category');
		$this->truncateTable('product');

		$content = [
			'食品'  => [
				['Apple', 'apple.jpg'],
				['Bread', 'bread.jpg'],
				['Chips', 'chips.jpg'],
				['Chocolate', 'chocolate.jpg'],
				['Eggs', 'eggs.jpg'],
				['Milk', 'milk.jpg'],
				['Oil', 'oil.jpg'],
				['Snack', 'snack.jpg'],
			],
			'日用品' => [
				['Gum tape', 'i_gum_tape.jpg'],
				['Tooth paste', 'i_paste.jpg'],
				['Soap', 'i_soap.jpg'],
				['Toilet paper', 'i_toilet_paper.jpg'],
			],
			'その他' => [
			],
		];
		foreach ($content as $category => $products) {
			$this->insert('category', ['title' => $category]);
			$categoryId = Yii::$app->db->lastInsertID;
			foreach ($products as $product) {
				$this->insert('product', [
					'title'      => $product[0],
					'code'       => uniqid(),
					'price'      => rand(100, 500),
					'picture'    => $product[1],
					'categoryId' => $categoryId,
				]);
			}
		}
	}

	public function safeDown() {

	}
}
