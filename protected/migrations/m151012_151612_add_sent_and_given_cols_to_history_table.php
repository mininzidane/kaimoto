<?php

use app\models\History;

class m151012_151612_add_sent_and_given_cols_to_history_table extends \app\cli\Migration {

	public function safeUp() {
		$this->addColumn(History::tableName(), 'sent', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER cart');
		$this->addColumn(History::tableName(), 'given', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER sent');
	}

	public function safeDown() {
		$this->dropColumn(History::tableName(), 'given');
		$this->dropColumn(History::tableName(), 'sent');
	}
}
