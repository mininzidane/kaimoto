<?php

use app\models\History;

class m151111_015753_add_consumerId_to_history_table extends \app\cli\Migration {

	public function safeUp() {
		$this->addColumn(History::tableName(), 'customerId', 'INT(5) UNSIGNED NULL DEFAULT NULL AFTER userId');
	}

	public function safeDown() {
		$this->dropColumn(History::tableName(), 'customerId');
	}
}
