<?php

use \app\models\History;

class m151102_124654_add_creator_user_id_for_history extends \app\cli\Migration {

	public function safeUp() {
		$this->addColumn(History::tableName(), 'creatorUserId', 'INT(5) UNSIGNED NULL DEFAULT NULL AFTER userId');
	}

	public function safeDown() {
		$this->dropColumn(History::tableName(), 'creatorUserId');
	}
}
