<?php

use yii\db\Schema;

class m150909_125201_init_product_tables extends \app\cli\Migration {

	public function safeUp() {
		$this->createTable('category', [
			'id'      => 'INT(3) UNSIGNED NOT NULL AUTO_INCREMENT',
			'title'   => 'VARCHAR(255) NOT NULL',
			'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);
		$this->createTable('product', [
			'id'         => 'INT(5) UNSIGNED NOT NULL AUTO_INCREMENT',
			'title'      => 'VARCHAR(255) NOT NULL',
			'price'      => 'DECIMAL(6,2) NULL DEFAULT NULL',
			'picture'    => 'VARCHAR(255) NULL DEFAULT NULL',
			'categoryId' => 'INT(3) NULL DEFAULT NULL',
			'created'    => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);

		$content = [
			'Food'   => [
				['Cheeps', 100.55, 'water.jpg'],
				['Potato', 299.99, 'socks.jpg'],
			],
			'Drinks' => [
				['Beer', 500.55, 'brushes.jpg'],
				['Whiskey', 399.45, 'water.jpg'],
			],
			'Cloths' => [
				['Socks', 899.55, 'socks.jpg'],
				['Coat', 99.45, 'brushes.jpg'],
			],
		];
		foreach ($content as $category => $products) {
			$this->insert('category', ['title' => $category]);
			$categoryId = Yii::$app->db->lastInsertID;
			foreach ($products as $product) {
				$this->insert('product', [
					'title'      => $product[0],
					'price'      => $product[1],
					'picture'    => $product[2],
					'categoryId' => $categoryId,
				]);
			}
		}
	}

	public function safeDown() {
		$this->dropTable('product');
		$this->dropTable('category');
	}
}
