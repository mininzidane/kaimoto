<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Asset bundle for the Bower installed js/css files.
 */
class BowerAsset extends AssetBundle {

	public $basePath = '/test';
	public $sourcePath = '@bower';
	public $js = [
		'bootstrap/dist/js/bootstrap.min.js',
		'bootstrap-select/dist/js/bootstrap-select.min.js',
		'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
//		'jquery.cookie/jquery.cookie.js',
	];
	public $css = [
		'bootstrap-select/dist/css/bootstrap-select.min.css',
		'bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
	];
}
