<?php

namespace app\controllers;

use app\models\Category;
use app\models\Contractor;
use app\models\Customer;
use app\models\History;
use app\models\Product;
use app\models\StatisticsForm;
use app\models\User;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use app\models\LoginForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\data\Pagination;
use yii\image\drivers\Image;
use yii\image\drivers\Image_GD;
use yii\web\Cookie;
use yii\web\HttpException;

class MainController extends BaseController {

	protected $_perPage;

	public function init() {
		$this->_perPage = Yii::$app->params['perPage'];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
//			'captcha' => [
//				'class'           => 'yii\captcha\CaptchaAction',
//				'fixedVerifyCode' => YII_ENV_TEST? 'testme': null,
//			],
		];
	}

	public function actionIndex() {
		return $this->redirect(['catalog']);
	}

	public function actionCatalog() {
		$categories        = Category::find()->all();
		$this->view->title = '注文選択';
		return $this->render('catalog', [
			'categories' => $categories,
		]);
	}

	public function actionMail() {
		$this->view->title = '注文送信';
		if (Yii::$app->request->isPost && Yii::$app->request->post('clear') !== null) {
			Product::clearCart(Yii::$app->request->post('clear'));
		}

		$cart          = Product::getCart();
		$productGroups = Product::getGroupedByCompanyProducts($cart);

		$history         = new History();
		$history->cart   = $cart;
		$history->sent   = 1;
		$history->userId = Yii::$app->user->id;

		if (!count($cart)) {
			return $this->redirect(['catalog']);
		}

		if (Yii::$app->request->isPost) { // simple post is when user choose a customer
			// -- Save order cart
			$history->load(Yii::$app->request->post());
			// -- -- -- --
		}

		if (Yii::$app->request->isPost && Yii::$app->request->post('send') !== null) {
			$history->scenario = History::SCENARIO_SEND;

			if ($history->save()) {
				foreach (Yii::$app->request->post('company', []) as $i => $company) {
					$this->_sendHistoryEmail($company, $cart, $productGroups, $history, Yii::$app->request->post('comment')[$i]);
				}
				Product::clearCart();

				return $this->redirect(['main/history-edit', 'id' => $history->id, 'afterSent' => 1]);
			}
		}

		$params = Product::getJSONCart($cart, $productGroups, $history->getNumber(), @$history->customer->name, Yii::$app->user->getIdentity()->username);

		return $this->render('mail', [
			'cart'          => $cart,
			'productGroups' => $productGroups,
			'params'        => $params,
			'companies'     => Contractor::find()->indexBy('id')->all(),
			'history'       => $history,
		]);
	}

	public function actionLogin() {
		if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		$users = User::find()->all();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		} else {
			return $this->render('login', [
				'model' => $model,
				'users' => $users,
			]);
		}
	}

	public function actionLogout() {
		Yii::$app->user->logout();

		return $this->goHome();
	}

	private function _processHistoryPost() {
		if (Yii::$app->request->isPost) {
			$data = Yii::$app->request->post(StringHelper::basename(History::className()));

			if (array_key_exists('given', $data)) {
				/** @var History $history */
				$history        = History::findOne($data['given']);
				$history->given = 1;
				$history->save();
			}
		}
	}

	private function _actionHistory($histories, $perPage, $totalCount) {
		$pages = new Pagination(['totalCount' => $totalCount]);
		$pages->setPageSize($perPage);

		return $this->render('history', [
			'histories' => $histories,
			'pages'     => $pages,
		]);
	}

	public function actionMasterHistory($page = 1, $perPage = null, $sortDirection = 'desc') {
		if ($perPage === null) {
			$perPage = $this->_perPage;
		}
		$this->view->title = '履歴一覧';

		$this->_processHistoryPost();

		$searchQuery = Yii::$app->request->get('q');

		/** @var History[] $histories */
		$histories = History::find()
			->with('user')
			->searchByRelative(User::tableName(), 'creatorUserId', 'id', 'username', $searchQuery)
			->orderBy('created ' . strtoupper($sortDirection))
			->offset($perPage * ($page - 1))
			->limit($perPage)
			->all();

		$totalCount = History::find()
			->searchByRelative(User::tableName(), 'creatorUserId', 'id', 'username', $searchQuery)
			->count();

		return $this->_actionHistory($histories, $perPage, $totalCount);
	}

	public function actionHistory($page = 1, $perPage = null, $sortDirection = 'desc') {
		return $this->actionMasterHistory($page, $perPage, $sortDirection);
	}

	public function actionMaster() {
		$this->view->title = 'マスタ一覧';
		return $this->render('master');
	}

	public function actionSettings() {
		$this->view->title = '設定';
		return $this->render('settings');
	}

	public function actionUsers() {
		$this->view->title = 'スタッフマスター';

		$searchQuery = Yii::$app->request->get('q');
		$users       = User::find()
			->searchBy('username', $searchQuery)
			->all();

		return $this->render('users', [
			'users' => $users,
		]);
	}

	public function actionUsersAdd() {
		$user = new User();

		if (Yii::$app->request->isPost) {
			$user->attributes = Yii::$app->request->post(StringHelper::basename($user::className()));

			if ($user->save()) {
				return $this->redirect(['users']);
			}
		}

		return $this->render('users-add', [
			'user' => $user,
		]);
	}

	public function actionProducts($page = 1, $perPage = null) {
		if ($perPage === null) {
			$perPage = $this->_perPage;
		}
		$this->view->title = '商品一覧';

		$searchQuery = Yii::$app->request->get('q');
		$products    = Product::find()
			->with('category')
			->searchBy('title', $searchQuery)
			->offset($perPage * ($page - 1))
			->limit($perPage)
			->all();

		$pages = new Pagination(['totalCount' => Product::find()->searchBy('title', $searchQuery)->count()]);
		$pages->setPageSize($perPage);
		$this->_saveCurrentPage();
		return $this->render('products', [
			'products' => $products,
			'pages'    => $pages,
		]);
	}

	protected function _saveProductImage(Product $product) {
		$files = $_FILES[StringHelper::basename($product::className())];

		if (count($files['name']) == 0 || !$files['name']['picture']) {
			return;
		}

		/** @var Image_GD $image */
		$image = Yii::$app->image->load($files['tmp_name']['picture']);
		$image->resize(Yii::$app->params['productImageWidth'], Yii::$app->params['productImageHeight']);

		$ext = null;
		switch ($image->type) {
			case IMAGETYPE_JPEG:
			case IMAGETYPE_JPEG2000:
				$ext = 'jpg';
				break;
			case IMAGETYPE_GIF:
				$ext = 'gif';
				break;
			case IMAGETYPE_BMP:
				$ext = 'bmp';
				break;
			case IMAGETYPE_PNG:
				$ext = 'png';
				break;
		}
		$filename = $ext? md5_file($files['tmp_name']['picture']) . '.' . $ext: $files['name']['picture'];
		if ($image->save(Yii::getAlias('@webroot') . $product->getImagePath() . DIRECTORY_SEPARATOR . $filename)) {
			$product->picture = $filename;
		}
	}

	public function actionProductsEdit($id) {
		$this->view->title = '商品変更';
		/** @var Product $product */
		$product = Product::findOne($id);

		if (Yii::$app->request->isPost) {
			$product->attributes = Yii::$app->request->post(StringHelper::basename(get_class($product)));

			$this->_saveProductImage($product);

			if ($product->save()) {
				return $this->redirect(['products']);
			}
		}

		return $this->render('products-edit', [
			'product'     => $product,
			'categories'  => Category::find()->all(),
			'contractors' => Contractor::find()->all(),
			'currentPage' => $this->_getCurrentPage(),
		]);
	}

	public function actionProductsAdd() {
		$this->view->title = '商品追加';
		$product           = new Product();

		if (Yii::$app->request->isPost) {
			$requestIndex        = StringHelper::basename($product::className());
			$product->attributes = Yii::$app->request->post($requestIndex);

			$this->_saveProductImage($product);

			if ($product->save()) {
				return $this->redirect(['products']);
			}
		}

		return $this->render('products-edit', [
			'addMode'     => true,
			'product'     => $product,
			'categories'  => Category::find()->all(),
			'contractors' => Contractor::find()->all(),
		]);
	}

	public function actionHistoryShow($id) {
		/** @var History $history */
		$history       = History::findOne($id);
		$cart          = $history->cart;
		$productGroups = Product::getGroupedByCompanyProducts($cart);
		$params        = Product::getJSONCart($cart, $productGroups);

		return $this->render('history-show', [
			'cart'          => $cart,
			'productGroups' => $productGroups,
			'params'        => $params,
		]);
	}

	/**
	 *
	 *
	 * @author Lukin A.
	 *
	 * @param int     $company ID of company
	 * @param array   $cart
	 * @param array   $productGroups
	 * @param History $history model of history for sending date and number in PDF
	 * @param string  $comment
	 * @return bool
	 * @throws HttpException
	 */
	protected function _sendHistoryEmail($company, $cart, $productGroups, $history, $comment = '') {
		$contractor = Contractor::findOne($company); /** @var $contractor Contractor */
		$pdf = new \mPDF('','A4',0,'sun-exta');
		$pdf->WriteHTML(
			$this->render('@views/mail/mail', [
				'comment'    => $comment,
				'cart'       => $cart,
				'products'   => $productGroups[$company],
				'contractor' => $contractor,
				'history'    => $history,
			])
		);

		/** @var Contractor $contractor */
		$contractor = Contractor::findOne($company);
		if (!$contractor) {
			throw new HttpException(404, "Contractor with id = {$company} not found");
		}

		/** @var \app\models\User $user */
		$user = Yii::$app->user->getIdentity();
		$sent = Yii::$app->mailer
			->compose()
			->setTextBody($comment)
			->attachContent($pdf->Output('', 'S'), ['fileName' => 'attachment.pdf', 'contentType' => 'application/pdf'])
			->setFrom($user->senderEmail)
			->setTo(array_map('trim', explode(',', $contractor->email)))
			->setSubject('Send email kaimono')
			->send();

//		$transport = \Swift_SmtpTransport::newInstance('smtp.example.org', 25)
//				->setUsername('your username')
//				->setPassword('your password');
//		$mailer    = \Swift_Mailer::newInstance($transport);
//		// Create a message
//		$message = \Swift_Message::newInstance('Send email kaimono')
//				->setFrom($user->senderEmail)
//				->setTo(array_map('trim', explode(',', $contractor->email)))
//				->setBody('Here is the message itself');
//		$sent = $mailer->send($message);

		return $sent;
	}

	/**
	 *
	 *
	 * @author Lukin A.
	 *
	 * @param int  $id
	 * @param bool $sendNew   If set to true, then we create a clone history record from an old one
	 * @param bool $afterSent If send from /mail/ page. It used for disabling send button now
	 * @return string
	 * @throws HttpException
	 */
	public function actionHistoryEdit($id, $sendNew = false, $afterSent = false) {
		$this->view->title = '注文送信';

		/** @var History $history */
		$history = History::findOne($id);

		if ($sendNew) {
			$attributes          = $history->attributes;
			$history             = new History();
			$history->attributes = $attributes;
			$history->sent       = 0;
			$history->given      = 0;
			$history->cart       = $attributes['cart'];
		}

		if (!$history) {
			throw new HttpException(404, 'History record not found');
		}

		$cart          = $history->cart;
		$productGroups = Product::getGroupedByCompanyProducts($cart);

		if (Yii::$app->request->isPost) {
			// -- Delete from cart
			$deleteProducts = Yii::$app->request->post('deleteProducts', []);
			foreach ($deleteProducts as $deleteProductId) {
				unset($cart[$deleteProductId]);
			}
			if (count($deleteProducts)) {
				$history->cart = $cart;
				if (!$history->save()) {
					return $this->redirect(['history']);
				} elseif ($sendNew) {
					return $this->redirect(['history-edit', 'id' => $history->id]);
				}
			}
			// -- -- -- --

			// -- Save cart (and on send cart)
			if (Yii::$app->request->post('send') !== null || Yii::$app->request->post('save') !== null) {
				$productsToChange = Yii::$app->request->post('quantityProducts', []);

				if (count($productsToChange)) {
					$history->cart = Yii::$app->request->post('quantityProducts');
					$cart          = $history->cart; // recalculate cart
					$history->save();
				}
			}
			// -- -- -- --

			// -- Send cart
			if (Yii::$app->request->post('send') !== null) {
				$history->customerId = Yii::$app->request->post('History')['customerId']; // set (maybe) new customer from select
				$history->sent = 1;
				if ($history->save(true, ['sent', 'customerId']) && !$sendNew) {
					foreach (Yii::$app->request->post('company', []) as $company) {
						$this->_sendHistoryEmail($company, $cart, $productGroups, $history);
					}

					return $this->redirect(['history']);
				}
			}
			// -- -- -- --

			if ($sendNew) {
				return $this->redirect(['history-edit', 'id' => $history->id]);
			}
		}

		// Use $cart array after deleting only
		$productGroups = Product::getGroupedByCompanyProducts($cart);
		$params = Product::getJSONCart($cart, $productGroups, $history->getNumber(), @$history->customer->name, Yii::$app->user->getIdentity()->username);

		return $this->render('history-edit', [
			'cart'          => $cart,
			'productGroups' => $productGroups,
			'params'        => $params,
			'history'       => $history,
			'sendNew'       => $sendNew,
			'afterSent'     => $afterSent,
			'companies'     => Contractor::find()->all(),
		]);
	}

	/**
	 * @param $id
	 * @return string|\yii\web\Response
	 * @throws HttpException
	 */
	public function actionHistorySend($id) {
		/** @var History $history */
		$history       = History::findOne($id);
		$cart          = $history->cart;
		$productGroups = Product::getGroupedByCompanyProducts($cart);
		$params        = Product::getJSONCart($cart, $productGroups);

		if (Yii::$app->request->isPost && Yii::$app->request->post('send', null) !== null) {
			$history->customerId = Yii::$app->request->post('History')['customerId']; // set (maybe) new customer from select
			$history->sent = 1;

			if ($history->save(true, ['sent', 'customerId'])) {
				foreach (Yii::$app->request->post('company', []) as $company) {
					$this->_sendHistoryEmail($company, $cart, $productGroups, $history);
				}
				return $this->redirect(['history']);
			}
		}

		return $this->render('history-send', [
			'cart'          => $cart,
			'productGroups' => $productGroups,
			'params'        => $params,
			'history'       => $history,
		]);
	}

	public function actionContractors($page = 1, $perPage = null) {
		if ($perPage === null) {
			$perPage = $this->_perPage;
		}
		$this->view->title = '仕入マスタ';

		$searchQuery = Yii::$app->request->get('q');
		$contractors = Contractor::find()
			->searchBy('companyName', $searchQuery)
			->offset($perPage * ($page - 1))
			->limit($perPage)
			->all();

		$pages = new Pagination(['totalCount' => Contractor::find()->searchBy('companyName', $searchQuery)->count()]);
		$pages->setPageSize($perPage);
		return $this->render('contractors', [
			'contractors' => $contractors,
			'pages'       => $pages,
		]);
	}

	public function actionContractorsAdd() {
		$this->view->title = '仕入マスタ';
		$contractor        = new Contractor();

		if (Yii::$app->request->isPost) {
			$requestIndex           = StringHelper::basename($contractor::className());
			$contractor->attributes = Yii::$app->request->post($requestIndex);

			if ($contractor->save()) {
				return $this->redirect(['contractors']);
			}
		}

		return $this->render('contractors-edit', [
			'contractor' => $contractor,
			'addMode'    => true,
		]);
	}

	public function actionContractorsEdit($id) {
		$this->view->title = '';
		/** @var Contractor $contractor */
		$contractor = Contractor::findOne($id);

		if (Yii::$app->request->isPost) {
			$contractor->attributes = Yii::$app->request->post(StringHelper::basename(get_class($contractor)));

			if ($contractor->save()) {
				return $this->redirect(['contractors']);
			}
		}

		return $this->render('contractors-edit', [
			'contractor' => $contractor,
		]);
	}

	public function actionCustomers($page = 1, $perPage = null) {
		if ($perPage === null) {
			$perPage = $this->_perPage;
		}
		$this->view->title = 'お客様マスタ';

		$searchQuery = Yii::$app->request->get('q');
		$customers   = Customer::find()
			->searchBy('name', $searchQuery)
			->offset($perPage * ($page - 1))
			->limit($perPage)
			->all();

		$pages = new Pagination(['totalCount' => Customer::find()->searchBy('name', $searchQuery)->count()]);
		$pages->setPageSize($perPage);
		return $this->render('customers', [
			'customers' => $customers,
			'pages'     => $pages,
		]);
	}

	public function actionCustomersAdd() {
		$this->view->title = 'お客様マスタ';
		$customer          = new Customer();

		if (Yii::$app->request->isPost) {
			$requestIndex         = StringHelper::basename($customer::className());
			$customer->attributes = Yii::$app->request->post($requestIndex);

			if ($customer->save()) {
				return $this->redirect(['customers']);
			}
		}

		return $this->render('customers-edit', [
			'customer' => $customer,
			'addMode'  => true,
		]);
	}

	public function actionCustomersEdit($id) {
		$this->view->title = '';
		/** @var Contractor $customer */
		$customer = Customer::findOne($id);

		if (Yii::$app->request->isPost) {
			$customer->attributes = Yii::$app->request->post(StringHelper::basename(get_class($customer)));

			if ($customer->save()) {
				return $this->redirect(['customers']);
			}
		}

		return $this->render('customers-edit', [
			'customer' => $customer,
		]);
	}

	public function actionStatistics() {
		$this->view->title = 'レポート';

		$statisticsForm = new StatisticsForm();

		if (Yii::$app->request->isPost) { // if form is submitted, load new stat data and store it in session
			$statisticsForm->load(Yii::$app->request->post());
			$statisticProducts = $statisticsForm->getStatisticProducts();
			Yii::$app->session->set(self::STATISTIC_PRODUCT_SESSION_KEY, $statisticProducts);
		} else { // otherwise try to get stored stat data value or use an empty array
			$statisticProducts = Yii::$app->session->get(self::STATISTIC_PRODUCT_SESSION_KEY, []);
		}

		return $this->render('statistics', [
			'statisticsForm'    => $statisticsForm,
			'statisticProducts' => $statisticProducts,
			'users'             => User::find()->all(),
			'contractors'       => Contractor::find()->all(),
		]);
	}

	public function actionStatisticsCreateReport() {
		$statisticProducts = Yii::$app->session->get(self::STATISTIC_PRODUCT_REPORT_SESSION_KEY);

		return $this->render('statistics-create-report', [
			'statisticProducts' => $statisticProducts,
		]);
	}

	public function actionSaveStatisticReport() {
		Yii::$app->params['showTopMenu'] = false;
		$statisticProducts = Yii::$app->session->get(self::STATISTIC_PRODUCT_REPORT_SESSION_KEY, []);

		$pdf = new \mPDF('','A4',0,'sun-exta');
		$pdf->WriteHTML(
			$this->render('statistics-create-report', [
				'statisticProducts' => $statisticProducts,
			])
		);

		header('Content-type:application/pdf');
		header('Content-Disposition:attachment;filename=' . date('Ymd') . '_statistic_report.pdf');
		return $pdf->Output('', 'S');
	}
}