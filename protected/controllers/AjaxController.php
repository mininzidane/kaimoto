<?php

namespace app\controllers;

use app\models\Category;
use app\models\Contractor;
use app\models\Customer;
use app\models\History;
use app\models\Product;
use app\models\StatisticsForm;
use app\models\User;
use Yii;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

class AjaxController extends BaseController {

	public $layout = false;

	public function actionAddToCart($productId) {
		Product::addToCart($productId);
	}

	public function actionRemoveFromCart() {
		$productIds = Yii::$app->request->get('productId');
		if ($productIds) {
			Product::removeFromCart($productIds);
		}
	}

	public function actionGetCart() {
		$cart = Product::getCart();

		$productsInCartOrdered = [];
		if (count($cart)) {
			$productIds = array_keys($cart);
			$productsInCart = Product::find()->where(['id' => $productIds])->indexBy('id')->all();
			foreach ($productIds as $productId) {
				$productsInCartOrdered[$productId] = $productsInCart[$productId];
			}
			unset($productsInCart); // save memory)))
		}

		return $this->render('@parts/cart', [
			'products' => $productsInCartOrdered,
			'cart'     => $cart,
			'sum'      => Product::getCartSum(),
		]);
	}

	public function actionChangeQuantity($productId, $quantity) {
		$cart             = Product::getCart();
		$cart[$productId] = $quantity;
		Product::setCart($cart);
	}

	public function actionSaveHistory() {
		$history         = new History();
		$history->cart   = serialize(Product::getCart());
		if ($history->save()) {
			// -- Demo saving to the PDF (maybe not demo =)))
			/*$cart     = Product::getCart();
			$products = Product::findAll(array_keys($cart));

			$pdf = new \mPDF();
			$pdf->WriteHTML(
				$this->render('@views/mail/mail', [
					'comment'  => '',
					'cart'     => $cart,
					'products' => $products,
				])
			);
			$pdfPath = implode(DIRECTORY_SEPARATOR, [
				Yii::getAlias('@webroot'),
				'pdf',
			]);
			if (!file_exists($pdfPath)) {
				@mkdir($pdfPath);
			}
			$pdfName = "cart_{$history->id}.pdf";
			@file_put_contents(implode(DIRECTORY_SEPARATOR, [
				$pdfPath,
				$pdfName,
			]), $pdf->Output('', 'S'));*/
			// -- -- -- --

			Product::clearCart();
			return $this->redirect(['main/history']);
		}
		return $this->redirect(['main/catalog']);
	}

	public function actionGetCounter() {
		return $this->render('@parts/cart-counter');
	}

	public function actionGetJson() {
		$params = Yii::$app->request->get('params');
		return $this->renderJson(json_decode($params, true));
	}

	public function actionDeleteProduct($id) {
		return (int) Product::findOne($id)->delete();
	}

	public function actionDeleteUser($id) {
		if (User::find()->count() < 2) {
			return 0;
		}
		return (int) User::findOne($id)->delete();
	}

	public function actionChangeUser() {
		$attributes = Yii::$app->request->post(null, []);
		if (count($attributes) == 0) {
			Yii::$app->end();
		}

		/** @var User $user */
		$user = User::findOne($attributes['id']);
		$user->save(true, array_keys($attributes));

		// -- We can not switch off rules for password only, so copy password if exists
		if (isset($attributes['password']) && $attributes['password'] !== '') {
			$attributes['passwordRepeat'] = $attributes['password'];
		}
		// -- -- -- --

		$user->attributes = $attributes;
		return (int) $user->save(true, array_keys($attributes));
	}

	public function actionDeleteHistory($id) {
		return (int) History::findOne($id)->delete();
	}

	public function actionHistoryChangeQuantity($historyId, $productId, $quantity) {
		/** @var History $history */
		$history = History::findOne($historyId);
		return (int) $history->setProductQuantity($productId, $quantity);
	}

	public function actionContractorsDelete($id) {
		return (int) Contractor::findOne($id)->delete();
	}

	public function actionCustomersDelete($id) {
		return (int) Customer::findOne($id)->delete();
	}

	public function actionGetCustomers($q = null) {
		$sql    = "SELECT id, name FROM " . Customer::tableName();
		$params = [];
		if ($q !== null) {
			$sql .= " WHERE name LIKE :name";
			$params = [':name' => "%{$q}%"];
		}
		$customers = \Yii::$app->db->createCommand($sql, $params)->queryAll();

		$html = Html::tag('option', '');
		foreach ($customers as $customer) {
			$html .= "\n" . Html::tag('option', $customer['name'], ['value' => $customer['id']]);
		}
		return $html;
	}

	public function actionGetContractors($q = null) {
		$sql    = "SELECT id, companyName FROM " . Contractor::tableName();
		$params = [];
		if ($q !== null) {
			$sql .= " WHERE companyName LIKE :name";
			$params = [':name' => "%{$q}%"];
		}
		$contractors = \Yii::$app->db->createCommand($sql, $params)->queryAll();

		$html = Html::tag('option', '');
		foreach ($contractors as $contractor) {
			$html .= "\n" . Html::tag('option', $contractor['companyName'], ['value' => $contractor['id']]);
		}
		return $html;
	}

	/**
	 * Set form values to the session to view the statistic report
	 *
	 * @param string $type report type
	 * @param string $customer just a label
	 * @param string $contractor just a label
	 * @param string $member value of chosen type and label
	 */
	public function actionStatisticChangeType($type, $customer, $contractor, $member) {
		$statisticProducts = Yii::$app->session->get(self::STATISTIC_PRODUCT_SESSION_KEY, []);
		if (count($statisticProducts) == 0) {
			return;
		}

		$pageTitle = $pageName = '';
		switch ($type) {
			case StatisticsForm::TYPE_CONTRACTOR:
				$pageTitle = '受領書';
				$pageName = $contractor;
				break;
			case StatisticsForm::TYPE_CUSTOMER:
				$pageTitle = '注文書';
				$pageName = $customer;
				break;
		}
		foreach ($statisticProducts as &$statisticProduct) {
			$statisticProduct['pageTitle'] = $pageTitle;
			$statisticProduct['pageName']  = $pageName;
			$statisticProduct['member']    = $member;
			$statisticProduct['type']      = $type;
		}
		unset($statisticProduct);

		$statisticProducts = StatisticsForm::recalculatePrices($statisticProducts);
		Yii::$app->session->set(self::STATISTIC_PRODUCT_REPORT_SESSION_KEY, $statisticProducts);
	}
}
