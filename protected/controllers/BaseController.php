<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\User;

class BaseController extends Controller {

	const KEY_CURRENT_PAGE                     = 'current_page';
	const STATISTIC_PRODUCT_SESSION_KEY        = 'statisticProducts';
	const STATISTIC_PRODUCT_REPORT_SESSION_KEY = 'statisticProductsReport';

	public $enableCsrfValidation = false;
	public $layout = 'main';

	/**
	 * Get last pagination page number from session
	 *
	 * @author Lukin A.
	 *
	 * @return int
	 */
	protected function _getCurrentPage() {
		return Yii::$app->session->get(self::KEY_CURRENT_PAGE);
	}

	/**
	 * Saves last pagination page number stored in $_GET param 'page' into session
	 *
	 * @author Lukin A.
	 */
	protected function _saveCurrentPage() {
		Yii::$app->session->set(self::KEY_CURRENT_PAGE, Yii::$app->request->get('page'));
	}

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['login', 'logout', 'error'],
						'allow'   => true,
					],
					[
						'allow' => true,
						'roles' => [User::ROLE_ADMIN],
					],
					[
						// actions for admin only
						'actions' => [
							// users
							'users',
							'users-add',
							'delete-user',
							'change-user',

							// products
							'products',
							'products-add',
							'products-edit',
							'delete-product',

							// history
							'master-history',

							// master
							'contractors',
							'contractors-add',
							'customers',
							'customers-add',
						],
						'allow'   => false,
					],
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function beforeAction($action) {
		$this->view->title = Yii::$app->controller->action->id; // title by default
		return parent::beforeAction($action);
	}

	/**
	 * Echos json formatted data with proper headers
	 *
	 * @param array $data Array to serialize to JSON format
	 * @throws \yii\base\ExitException
	 * @return string
	 */
	public function renderJson(array $data) {
		header('Content-Type: application/json; charset=utf-8');
		return json_encode($data, defined('JSON_UNESCAPED_UNICODE')? JSON_UNESCAPED_UNICODE: 0);
	}
}
