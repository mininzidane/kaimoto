<?php
return [
    'adminActions' => [
        'type' => 2,
        'description' => 'Admin actions',
    ],
    'user' => [
        'type' => 1,
    ],
    'admin' => [
        'type' => 1,
        'children' => [
            'adminActions',
        ],
    ],
];
