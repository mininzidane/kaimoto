<?php

namespace app\models;

/**
 * @property int    $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $senderEmail
 * @property string $recipientEmails
 * @property string $role
 * @property string $locale
 * @property string $description
 *
 * @author  Lukin A.
 *
 * Class User
 * @package app\models
 */
class User extends ActiveRecord implements \yii\web\IdentityInterface {

	public $passwordRepeat;

	const SALT = 'me_cutie_super_safety_salt';

	const ROLE_USER  = 'user';
	const ROLE_ADMIN = 'admin';

	public static function find() {
		return new UserQuery(get_called_class());
	}

	public static function getRoles() {
		return [
			self::ROLE_USER,
			self::ROLE_ADMIN,
		];
	}

	public function rules() {
		return [
			[['username', 'password'], 'required'],
			[['username'], 'unique'],
			[['email', 'role', 'passwordRepeat'], 'safe'],
			['password', 'compare', 'compareAttribute' => 'passwordRepeat', 'skipOnEmpty' => true],
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id) {
		return self::find()->where('id = :id', [':id' => $id])->one();
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null) {
	}

	/**
	 * Finds user by username
	 *
	 * @param  string $username
	 * @return static|null
	 */
	public static function findByUsername($username) {
		return self::find()->where('username = :username', [':username' => $username])->one();
	}

	/**
	 * @inheritdoc
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey() {
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey) {
	}

	/**
	 * Validates password
	 *
	 * @param  string $password password to validate
	 * @return boolean if password provided is valid for current user
	 */
	public function validatePassword($password) {
		return $this->password === $this->getHashedPassword($password);
	}

	/**
	 * Return hashed password
	 *
	 * @author Lukin A.
	 *
	 * @param $password
	 * @return string
	 */
	public function getHashedPassword($password) {
		return md5(self::SALT . $password);
	}

	public function beforeSave($insert) {
		if (!parent::beforeSave($insert)) {
			return false;
		}

		$this->password = $this->getHashedPassword($this->password);
		return true;
	}
}

class UserQuery extends ActiveQuery {

}