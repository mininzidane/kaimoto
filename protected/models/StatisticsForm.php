<?php

namespace app\models;

use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class StatisticsForm extends Model {

	const TYPE_CUSTOMER   = 'customer';
	const TYPE_CONTRACTOR = 'contractor';

	public $customer;
	public $user;
	public $userChanged;
	public $contractor;
	public $regDateFrom;
	public $regDateTo;
	public $updateDateFrom;
	public $updateDateTo;
	public $product;
	public $type;
	public $given = false;

	public static function getTypesList() {
		return [
			self::TYPE_CUSTOMER   => '納品書',
			self::TYPE_CONTRACTOR => '受領書',
		];
	}

	/**
	 * This method is necessary because 'type' is set after report is done. And we need recalculate prices and sum due to type
	 *
	 * @param array $statisticProducts From @see self::getStatisticProducts()
	 * @return mixed
	 */
	public static function recalculatePrices($statisticProducts) {
		foreach ($statisticProducts as &$statisticProduct) {
			$statisticProduct['price'] = $statisticProduct['type'] == self::TYPE_CUSTOMER? $statisticProduct['price']: $statisticProduct['purchasePrice'];
			$statisticProduct['sum']   = $statisticProduct['quantity'] * $statisticProduct['price'];
		}
		return $statisticProducts;
	}

	/**
	 * Return cart array with format [
	 * [productId] inc => [
	 *     'number'   => '',
	 *     'customer' => '',
	 *     'user'     => '',
	 *     'given'    => '',
	 *     'created'  => '',
	 *     'id'       => '',
	 *     'title'    => '',
	 *     'company'  => '',
	 *     'quantity' => '',
	 *     'price'    => '',
	 *     'sum'      => '',
	 * ]
	 *
	 * @author Lukin A.
	 *
	 * @return array
	 */
	public function getStatisticProducts() {
		// check all fields to continue a function
//		if (!$this->customer && !$this->user && !$this->userChanged && !$this->contractor && !$this->product
//			&& !$this->regDateFrom && !$this->regDateTo && !$this->updateDateFrom && !$this->updateDateTo && !$this->given) {
//			return [];
//		}

		$histories = History::find();

		// -- Search by customer
		if ($this->customer) {
			$customerIds = \Yii::$app
				->db
				->createCommand('SELECT `id` FROM ' . Customer::tableName() . " WHERE `name` LIKE '%{$this->customer}%'")
				->queryColumn();
			if (count($customerIds)) {
				$histories->andWhere('customerId IN (' . implode(',', $customerIds) . ')');
			}
		}
		// -- -- -- --

		// -- Search by creator user
		if ($this->user) {
			$userIds = \Yii::$app
				->db
				->createCommand('SELECT `id` FROM ' . User::tableName() . " WHERE `username` LIKE '%{$this->user}%'")
				->queryColumn();
			if (count($userIds)) {
				$histories->andWhere('creatorUserId IN (' . implode(',', $userIds) . ')');
			}
		}
		// -- -- -- --

		// -- Search by user who updated history
		if ($this->userChanged) {
			$userIds = \Yii::$app
				->db
				->createCommand('SELECT `id` FROM ' . User::tableName() . " WHERE `username` LIKE '%{$this->userChanged}%'")
				->queryColumn();
			if (count($userIds)) {
				$histories->andWhere('userId IN (' . implode(',', $userIds) . ')');
			}
		}
		// -- -- -- --

		// -- Search by contractor
		if ($this->contractor) {
			// search for products of those contractors
			$contractorProductIds = \Yii::$app
				->db
				->createCommand('SELECT `id` FROM ' . Product::tableName() . ' WHERE `contractorId` = ' . $this->contractor)
				->queryColumn();

			$historyIds = History::findHistoryIdsByProductIds($contractorProductIds);
			$histories->andWhere('id IN (' . implode(',', $historyIds) . ')');
		}
		// -- -- -- --

		// -- Statistic conditions
		if ($this->regDateFrom) {
			$histories->andWhere("DATE(created) >= STR_TO_DATE('{$this->regDateFrom}', '%m/%d/%Y')");
		}
		if ($this->regDateTo) {
			$histories->andWhere("DATE(created) <= STR_TO_DATE('{$this->regDateTo}', '%m/%d/%Y')");
		}
		if ($this->updateDateFrom) {
			$histories->andWhere('modified >= :time', [':time' => strtotime($this->updateDateFrom)]);
		}
		if ($this->updateDateTo) {
			$histories->andWhere('modified <= :time', [':time' => strtotime($this->updateDateTo)]);
		}
		if ($this->given !== '') {
			$histories->andWhere('given = :given', [':given' => (int) $this->given]);
		}
		// -- -- -- --

		// -- Search by product
		if ($this->product) {
			$productIds = \Yii::$app
				->db
				->createCommand('SELECT `id` FROM ' . Product::tableName() . " WHERE `title` LIKE '%{$this->product}%'")
				->queryColumn();

			if (count($productIds)) {
				$historyIds = History::findHistoryIdsByProductIds($productIds);
				if (count($historyIds)) {
					$histories->andWhere('id IN (' . implode(',', $historyIds) . ')');
				}
			}
		}
		// -- -- -- --

		// -- Fill array with history data
		$statisticProducts = $statisticHistories = [];
		$histories = $histories->all(); /** @var History[] $histories */

		foreach ($histories as $history) {
			if ($history->cart) {
				$statisticHistories[$history->id] = [
					'id'       => $history->id,
					'created'  => $history->created,
					'number'   => $history->getNumber(),
					'customer' => @$history->customer->name,
					'user'     => @$history->user->username,
					'given'    => $history->given,
					'cart'     => $history->cart,
					'index'    => $history->getIndexForCurrentDay(),
				];
			}
		}
		// -- -- -- --

		// -- Fill array with product data
		foreach ($statisticHistories as $historyId => &$statisticHistory) {
			/** @var Product[] $products */
			$products = Product::find()
				->where(['id' => array_keys($statisticHistory['cart'])])
				->indexBy('id')
				->all();

			if (count($products) == 0) {
				continue;
			}

			$i = 1;
			foreach ($statisticHistory['cart'] as $productId => $quantity) {
				if ($this->product && !in_array($productId, $productIds)) { // if used search with product, we need to add only checked products
					continue;
				}
				if ($this->contractor && !in_array($productId, $contractorProductIds)) { // if used search with contractor, we need to add only products with that contractor
					continue;
				}

				$statisticProducts[] = [
					'pageTitle' => '', // duplicate titles, but i don't care
					'pageName'  => '',

					'id'        => $statisticHistory['id'],
					'number'    => $statisticHistory['number'],
					'customer'  => $statisticHistory['customer'],
					'user'      => $statisticHistory['user'],
					'given'     => $statisticHistory['given'],
					'created'   => $statisticHistory['created'],

					'title'         => $products[$productId]->title,
					'company'       => @$products[$productId]->company->companyName,
					'quantity'      => $quantity,
					'price'         => $products[$productId]->price,
					'purchasePrice' => $products[$productId]->purchasePrice,
					'sum'           => $quantity * $products[$productId]->price,
					'index'         => $i++,
					'type'          => $this->type,
				];
			}
		}
		// -- -- -- --

//		$statisticProducts = self::recalculatePrices($statisticProducts);
		return $statisticProducts;
	}

	public function rules() {
		return [
			[['query'], 'required', 'message' => 'Client not selected'],
			[['customer', 'user', 'userChanged', 'contractor', 'regDateFrom', 'regDateTo', 'updateDateFrom', 'updateDateTo', 'product', 'given', 'type'], 'safe'],
		];
	}

	public function attributeLabels() {
		return [
			'customer'       => '利用者',
			'user'           => '担当者',
			'userChanged'    => '更新者',
			'contractor'     => '仕入先',
			'regDateFrom'    => '登録日',
			'updateDateFrom' => '更新日',
			'product'        => '商品',
			'given'          => '届いた',
			'type'           => 'タイトル',
		];
	}
}
