<?php

namespace app\models;

use \Yii;

/**
 * @property int    $id
 * @property string $companyName
 * @property string $email
 * @property string $created
 *
 * @author  Lukin A.
 *
 * Class Contractor
 * @package app\models
 */
class Contractor extends ActiveRecord {

	public static function find() {
		return new ContractorQuery(get_called_class());
	}

	public function attributeLabels() {
		return [
		];
	}

	public function rules() {
		return [
			[['companyName', 'email'], 'required'],
			['email', 'email'],
		];
	}
}

class ContractorQuery extends ActiveQuery {

}