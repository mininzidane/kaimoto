<?php

namespace app\models;

/**
 * @property Product[] $products
 * @property string $title
 *
 * @author  Lukin A.
 *
 * Class Category
 * @package app\models
 */
class Category extends ActiveRecord {

	public function getProducts() {
		return $this->hasMany(Product::className(), ['categoryId' => 'id']);
	}
}
