<?php

namespace app\models;

use \Yii;
use yii\helpers\ArrayHelper;

/**
 * @property int        $id
 * @property string     $title
 * @property string     $code
 * @property int        $price
 * @property int        $purchasePrice
 * @property string     $picture
 * @property int        $contractorId
 * @property int        $categoryId
 *
 * @property string     $imageUrl
 * @property Category   $category
 * @property Contractor $company
 *
 * @author  Lukin A.
 *
 * Class Product
 * @package app\models
 */
class Product extends ActiveRecord {

	const SESSION_KEY_CART = 'cart';

	public static function find() {
		return new ProductQuery(get_called_class());
	}

	public function attributeLabels() {
		return [
			'title'         => '商品名',
			'price'         => '値段 (税抜き)',
			'purchasePrice' => '仕入れ値',
			'picture'       => '画像',
			'contractorId'  => '供給者',
			'categoryId'    => 'カテゴリー',
			'code'          => '商品コード',
		];
	}

	/**
	 * [
	 *    [productId => quantity],
	 *    ...
	 *    [...],
	 * ]
	 *
	 * @author Lukin A.
	 *
	 * @return array
	 */
	public static function getCart() {
		return Yii::$app->session->get(self::SESSION_KEY_CART, []);
	}

	/**
	 *
	 *
	 * @author Lukin A.
	 *
	 * @param array $value
	 */
	public static function setCart($value) {
		Yii::$app->session->set(self::SESSION_KEY_CART, $value);
	}

	/**
	 *
	 *
	 * @author Lukin A.
	 *
	 * @param int $productId
	 */
	public static function addToCart($productId) {
		$cart = self::getCart();
		if (array_key_exists($productId, $cart)) {
			$cart[$productId]++;
		} else {
			$cart = $cart + [$productId => 1];
		}
		self::setCart($cart);
	}

	/**
	 *
	 *
	 * @author Lukin A.
	 *
	 * @param array|int $productIds
	 */
	public static function removeFromCart($productIds) {
		$cart = self::getCart();
		if (!is_array($productIds)) {
			$productIds = [$productIds];
		}
		foreach ($productIds as $productId) {
			if (array_key_exists($productId, $cart)) {
				unset($cart[$productId]);
			}
		}
		self::setCart($cart);
	}

	/**
	 *
	 *
	 * @author Lukin A.
	 *
	 * @return float|int
	 */
	public static function getCartSum() {
		$cart = self::getCart();

		if (count($cart) == 0) {
			return 0;
		}

		$sum = 0;
		/** @var Product[] $productsInCart */
		$productsInCart = Product::findAll(array_keys($cart));
		foreach ($productsInCart as $product) {
			$sum += $product->price * $cart[$product->id];
		}
		return $sum;
	}

	/**
	 * Clear all cart if $company param is not specified or clear products by $company
	 *
	 * @author Lukin A.
	 *
	 * @param string|null $contractorId
	 */
	public static function clearCart($contractorId = null) {
		if ($contractorId === null) {
			Yii::$app->session->remove(self::SESSION_KEY_CART);
		} else {
			$cart        = self::getCart();
			$products    = self::find()
				->where('contractorId = :contractorId AND id IN (' . implode(',', array_keys($cart)) . ')')
				->params([':contractorId' => $contractorId])
				->all();
			$productsIds = ArrayHelper::getColumn($products, 'id');
			foreach ($productsIds as $productsId) {
				unset($cart[$productsId]);
			}

			if (count($cart)) {
				self::setCart($cart);
			} else {
				self::clearCart();
			}
		}
	}

	/**
	 * Get grouped by company product model array.
	 * Array in format: [
	 *    company1 => [productModel-1-1, productModel-1-2, ...],
	 *    company2 => [productModel-2-1, productModel-2-2, ...]
	 *    ...
	 * ]
	 *
	 * @author Lukin A.
	 *
	 * @param array $cart
	 * @return array
	 */
	public static function getGroupedByCompanyProducts($cart = null) {
		if ($cart === null) {
			$cart = self::getCart();
		}
		$products = self::findAll(array_keys($cart));

		$productGroups = [];
		foreach ($products as $product) {
			$productGroups[$product->company? $product->company->id: 0][] = $product;
		}
		return $productGroups;
	}

	/**
	 * Get grouped by company json decoded params from cart
	 *
	 * @author Lukin A.
	 *
	 * @param array $cart
	 * @param array $productGroups    Array in format: [
	 *                                company1 => [productModel-1-1, productModel-1-2, ...],
	 *                                company2 => [productModel-2-1, productModel-2-2, ...]
	 *                                ...
	 *                                ]
	 * @return array
	 */
	public static function getGroupedByCompanyJSONCart($cart = null, $productGroups = null) {
		if ($cart === null) {
			$cart = self::getCart();
		}
		if ($productGroups === null) {
			$productGroups = self::getGroupedByCompanyProducts();
		}

		$paramGroups = [];
		foreach ($productGroups as $company => $group) {
			$productsJson = [];
			$total        = 0;
			foreach ($group as $product) {
				/** @var Product $product */
				$productsJson['cart'][] = [
					'title'    => $product->title,
					'price'    => $product->price,
					'quantity' => $cart[$product->id],
				];
				$total += $product->price * $cart[$product->id];
			}
			$productsJson['total'] = $total;
			$paramGroups[$company] = json_encode($productsJson);
		}
		return $paramGroups;
	}

	public static function getJSONCart($cart = null, $productGroups = null, $orderNumber = null, $customerName = null, $username = null) {
		if ($cart === null) {
			$cart = self::getCart();
		}
		if ($productGroups === null) {
			$productGroups = self::getGroupedByCompanyProducts($cart);
		}

		$params = [];
		$total  = 0;
		foreach ($productGroups as $company => $group) {
			foreach ($group as $product) {
				/** @var Product $product */
				$params['cart'][] = [
					'title'    => $product->title,
					'price'    => $product->price,
					'quantity' => $cart[$product->id],
				];
				$total += $product->price * $cart[$product->id];
			}
		}
		$params['total'] = $total;

		if ($orderNumber !== null) {
			$params['orderNumber'] = $orderNumber;
		}
		if ($customerName !== null) {
			$params['customerName'] = $customerName;
		}
		if ($username !== null) {
			$params['username'] = $username;
		}
		return json_encode($params);
	}

	public function rules() {
		return [
			['title', 'required', 'message' => '商品名を記入してください'],
			['price', 'required', 'message' => '値段 (税抜き)を記入してください'],
			['contractorId', 'required', 'message' => '供給者を選んでください'],
			['categoryId', 'required', 'message' => '商品カテゴリーを選んでください'],
			['code', 'required', 'message' => '商品コードを記入してください'],
			[['price', 'categoryId'], 'number', 'integerOnly' => true],
			[['code', 'purchasePrice'], 'safe'],
		];
	}

	public function getCategory() {
		return $this->hasOne(Category::className(), ['id' => 'categoryId']);
	}

	public function getCompany() {
		return $this->hasOne(Contractor::className(), ['id' => 'contractorId']);
	}

	public function getImagePath() {
		return implode(DIRECTORY_SEPARATOR, ['', 'images', 'products']);
	}

	/**
	 *
	 *
	 * @author Lukin A.
	 *
	 * @return string
	 */
	public function getImageUrl() {
		return $this->getImagePath() . DIRECTORY_SEPARATOR . $this->picture;
	}

	/**
	 *
	 *
	 * @author Lukin A.
	 *
	 * @return bool
	 */
	public function checkIsInCart() {
		return in_array($this->id, array_keys(self::getCart()));
	}
}

class ProductQuery extends ActiveQuery {

}