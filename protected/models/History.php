<?php

namespace app\models;

use \Yii;

/**
 * @property int    $id
 * @property array  $cart
 * @property bool   $sent
 * @property bool   $given
 * @property int    $userId
 * @property int    $customerId
 * @property int    $creatorUserId
 * @property string $created
 * @property string $modified
 *
 * @property Customer $customer
 * @property User     $creator
 * @property User     $user
 *
 * @author  Lukin A.
 *
 * Class History
 * @package app\models
 */
class History extends ActiveRecord {

	const SCENARIO_SEND = 'send';

	private $_indexForCurrentDay;

	public static function find() {
		return new HistoryQuery(get_called_class());
	}

	/**
	 * Search History records by product ids in their carts
	 *
	 * @author Lukin A.
	 *
	 * @param array  $productIds
	 * @return array $historyIds History DB records ids which contains product ids in their carts
	 */
	public static function findHistoryIdsByProductIds(array $productIds) {
		if (count($productIds) == 0) {
			return [];
		}

		$data = Yii::$app
			->db
			->createCommand('SELECT `id`, `cart` FROM ' . self::tableName())
			->queryAll();

		$historyIds = [];
		foreach ($data as $row) {
			$cart = unserialize($row['cart']);
			// look for intersection of cart product ids and passed product ids
			if (count(array_intersect(array_keys($cart), $productIds))) {
				$historyIds[] = $row['id'];
			}
		}
		return $historyIds;
	}

	public function rules() {
		return [
			[['customerId'], 'required', 'on' => [self::SCENARIO_SEND]],
			[['sent', 'given'], 'in', 'range' => [0, 1]],
			[['customerId'], 'safe'],
		];
	}

	public function beforeSave($insert) {
		if (!parent::beforeSave($insert)) {
			return false;
		}

		// remove record if cart is empty
		if (count($this->cart) == 0) {
			$this->delete();
			return false;
		}

		// -- Serialize cart array for saving in DB
		if (is_array($this->cart)) {
			$this->cart = @serialize($this->cart);
		}
		// -- -- -- --

		if ($this->isNewRecord) {
			$this->creatorUserId = Yii::$app->user->id;
		} else {
			$this->userId = Yii::$app->user->id;
			$this->modified = time(); // Save modified date
		}

		// save user who make change
		return true;
	}

	public function afterFind() {
		if (is_string($this->cart)) {
			$this->cart = @unserialize($this->cart);
		}
		parent::afterFind();
	}

	public function getCustomer() {
		return $this->hasOne(Customer::className(), ['id' => 'customerId']);
	}

	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'userId']);
	}

	public function getCreator() {
		return $this->hasOne(User::className(), ['id' => 'creatorUserId']);
	}

	public function setProductQuantity($productId, $quantity) {
		$cart = $this->cart;
		$cart[$productId] = $quantity;
		$this->cart = $cart;
		return $this->save();
	}

	/**
	 * Get index of history for current day
	 *
	 * @author Lukin A.
	 *
	 * @return int
	 */
	public function getIndexForCurrentDay() {
		if ($this->_indexForCurrentDay === null) {
			$models                    = self::find()
				->where(['DATE(created)' => date('Y-m-d', strtotime($this->created))])
				->orderBy('created ASC')
				->indexBy('id')
				->all();

			// no models - be the first
			if (count($models) == 0) {
				return ($this->_indexForCurrentDay = 1);
			}

			// not yet saved to DB model - get last number + 1
			if ($this->id === null) {
				return ($this->_indexForCurrentDay = count($models) + 1);
			}

			$index                     = array_search($this->id, array_keys($models));
			$this->_indexForCurrentDay = ++$index;
		}
		return (int) $this->_indexForCurrentDay;
	}

	/**
	 * Get special format number contains date and increment
	 *
	 * @author Lukin A.
	 *
	 * @return string
	 */
	public function getNumber() {
		return date('Ymd', $this->created? strtotime($this->created): time()) . str_pad($this->getIndexForCurrentDay(), 4, '0', STR_PAD_LEFT);
	}

	public function attributeLabels() {
		return [
			'customerId' => 'お客様ID',
		];
	}

}

class HistoryQuery extends ActiveQuery {

	public function init() {
		$this->orderBy("created DESC");
		parent::init();
	}

	public function own() {
		$this->andWhere(['userId' => Yii::$app->user->id]);
		return $this;
	}
}
