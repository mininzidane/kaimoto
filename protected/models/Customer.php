<?php

namespace app\models;

use \Yii;

/**
 * @property int    $id
 * @property string $name
 * @property string $address
 * @property string $number
 * @property string $created
 *
 * @author  Lukin A.
 *
 * Class Customer
 * @package app\models
 */
class Customer extends ActiveRecord {

	public static function find() {
		return new CustomerQuery(get_called_class());
	}

	public function attributeLabels() {
		return [
		];
	}

	public function rules() {
		return [
			[['name', 'address', 'number'], 'required'],
		];
	}
}

class CustomerQuery extends ActiveQuery {

}