<?php

namespace app\models;

class ActiveQuery extends \yii\db\ActiveQuery {

	public function getTableName() {
		$modelName = $this->modelClass;
		return $modelName::tableName();
	}

	public function resetScopes() {
		$this->where = '';
		return $this;
	}

	/**
	 * Filtering using LIKE with start and end
	 *
	 * @author Lukin A.
	 *
	 * @param string $attribute
	 * @param string $value
	 * @return $this
	 */
	public function searchBy($attribute, $value) {
		if ($value) {
			$this->andWhere("{$attribute} LIKE :value", [':value' => "%{$value}%"]);
		}
		return $this;
	}

	/**
	 * Filtering in linked table using LIKE with start and end
	 *
	 * @author Lukin A.
	 *
	 * @param string $relativeTable Linked table name
	 * @param string $linkFrom      Link key from model table
	 * @param string $linkTo        Key in relative table to connect for
	 * @param string $attribute     Relative column name for query value
	 * @param string $value         Value to search for (LIKE %{value}%)
	 * @return $this
	 */
	public function searchByRelative($relativeTable, $linkFrom, $linkTo, $attribute, $value) {
		if (!$value) {
			return $this;
		}

		// -- get constraint keys for 'like' condition in relative table
		$sql = "SELECT DISTINCT {$linkFrom} FROM " . $this->getTableName() . "AS this
		INNER JOIN {$relativeTable} AS rel ON this.{$linkFrom} = rel.{$linkTo}
		WHERE rel.{$attribute} LIKE :value";
		$filteredLinks = \Yii::$app->db->createCommand($sql, [':value' => "%{$value}%"])->queryColumn();
		// -- -- -- --

		$this->andWhere("{$linkFrom} IN (" . implode(',', count($filteredLinks)? $filteredLinks: [0]) . ")");
		return $this;
	}
}