<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\User;

class RbacController extends Controller {

	public function actionInit() {

		$auth  = Yii::$app->authManager;

		// add admin permission
		$adminActions              = $auth->createPermission('adminActions');
		$adminActions->description = 'Admin actions';
		$auth->add($adminActions);

		// add "editProduct" permission
//		$editProduct              = $auth->createPermission('editProduct');
//		$editProduct->description = 'Edit product';
//		$auth->add($editProduct);

		// add "user" role and give this role the "createPost" permission
		$user = $auth->createRole(User::ROLE_USER);
		$auth->add($user);
//		$auth->addChild($user, $editProduct);

		// add "admin" role and give this role the "updatePost" permission
		// as well as the permissions of the "author" role
		$admin = $auth->createRole(User::ROLE_ADMIN);
		$auth->add($admin);
		$auth->addChild($admin, $adminActions);
	}
}