<?php

namespace app\components;

use app\models\Customer;
use yii\base\Widget;

class CustomerSelectWidget extends Widget {

	/** @var \yii\bootstrap\ActiveForm */
	public $form;
	/** @var \app\models\History */
	public $history;
	/** @var bool Whether to submit by change event on select */
	public $withoutPageReloading = false;

	public $options = [];

	public function run() {
		return $this->render('@parts/customer-select', [
			'form'                 => $this->form,
			'history'              => $this->history,
			'customers'            => Customer::find()->all(),
			'withoutPageReloading' => $this->withoutPageReloading,
			'options'              => $this->options,
		]);
	}
}
