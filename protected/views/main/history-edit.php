<?php

use \yii\helpers\Html;
use \yii\helpers\Url;
use \yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\components\CustomerSelectWidget;

/**
 * @var  yii\web\View          $this
 * @var  \app\models\Product[] $products
 * @var array                  $cart
 * @var array                  $productGroups
 * @var string                 $params
 * @var \app\models\History    $history
 * @var bool                   $sendNew
 * @var bool                   $afterSent
 **/

$this->registerJsFile('js/history.js', ['depends' => ['yii\web\JqueryAsset']]);

/** @var \app\models\User $user */
$user = Yii::$app->user->getIdentity();

$companies = ArrayHelper::map($companies, 'id', 'email');
?>

<?php $form = ActiveForm::begin(['options' => ['method' => 'post']]); ?>

	<?php if (!$afterSent) { ?>
		<h4>No. <?= $history->getNumber() ?></h4>

		<?php foreach ($productGroups as $company => $products) { ?>
			<input type="hidden" name="company[]" value="<?= $company ?>">

			<div class="shadowed-box padded-box">
				<p>送信者： <?= $user->senderEmail ?></p>

				<p>受信者： <?= @$companies[$company] ?></p>

				<p>注文：</p>

				<?= $this->render('@parts/product-table', [
					'products' => $products,
					'cart'     => $cart,
					'editMode' => true,
				]) ?>

				<div class="row">
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary button-blue button-blue_grey"
								name="delete"><span class="glyphicon glyphicon-remove"></span>破棄
						</button>
					</div>
					<div class="col-xs-8">
						<div class="pull-right">
							<?php if (!$sendNew) { ?>
								<button type="submit" class="btn btn-primary button-blue button-blue_grey"
										name="save"><span class="glyphicon glyphicon-save-file"></span>保存
								</button>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	<?php } ?>

	<div class="padded-box padded-box_no-margin-bottom light-grey-bg clearfix">
		<?php if ($afterSent) { ?>
			<h1>送信しました</h1>
		<?php } ?>

		<?= CustomerSelectWidget::widget([
			'form'                 => $form,
			'history'              => $history,
			'options'              => [
				'class'       => 'js-disable-on-change form-control',
				'data-target' => '[name=send]',
			],
		]) ?>

		<button type="submit" class="btn btn-primary button-blue pull-right js-disable-on-change" name="send" data-target="[name=send]"
			<?= $afterSent || !$history->customerId? 'disabled': ''?>><span
			class="glyphicon glyphicon-envelope"></span>送信</button>
		<a href='<?= Url::to(['ajax/get-json']) . '?params=' . $params ?>'
		   target="_blank"
		   class="btn btn-primary button-blue pull-right"><span class="glyphicon glyphicon-print"></span>プリントアウト</a>
		<a class="btn btn-primary button-blue button-blue_grey" href="<?= Url::to(['main/history']) ?>"><span
				class="icon-back"></span>戻る
		</a>
	</div>

<?php ActiveForm::end(); ?>