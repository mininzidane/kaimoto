<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use \yii\bootstrap\ActiveForm;
use app\models\User;

/**
 * @var  yii\web\View     $this
 * @var  \app\models\User $user
 **/

$roles = User::getRoles();
?>

<div class="shadowed-box padded-box">
	<?php
	$form                          = ActiveForm::begin();
	$form->fieldConfig['template'] = '<div class="row"><div class="col-sm-3">{label}{error}</div><div class="col-sm-9">{input}</div></div>';
	?>

	<?= $form->field($user, 'username') ?>
	<?= $form->field($user, 'password')->passwordInput() ?>
	<?= $form->field($user, 'passwordRepeat')->passwordInput() ?>
	<?= $form->field($user, 'role')->dropDownList(array_combine($roles, $roles)) ?>

	<div class="form-group">
		<?= Html::submitButton('保存', ['class' => 'btn btn-primary button-blue']) ?>
	</div>
</div>