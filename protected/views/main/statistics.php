<?php
use \yii\helpers\Html;
use \yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use \app\models\StatisticsForm;
use \yii\helpers\ArrayHelper;

/**
 * @var  yii\web\View              $this
 * @var \app\models\StatisticsForm $statisticsForm
 * @var array                      $statisticProducts
 * @var \app\models\User[]         $users
 * @var \app\models\Contractor[]   $contractors
 */
?>

<?php $form = ActiveForm::begin(); ?>

	<div class="shadowed-box padded-box">
		<?php
		$inputConfig                   = '<div class="row"><div class="col-sm-4 nobr">{label}</div><div class="col-sm-8">{input}{error}</div></div>';
		$leftDatePickerConfig          = '<div class="row"><div class="col-sm-6 nobr">{label}:</div><div class="col-sm-6"><div class="input-group">{input}<div class="input-group-addon addon-blue"><span class="glyphicon glyphicon-calendar"></span></div></div></div></div>';
		$rightDatePickerConfig         = '<div class="input-group">{input}<div class="input-group-addon addon-blue"><span class="glyphicon glyphicon-calendar"></span></div></div>';
		$form->fieldConfig['template'] = $inputConfig;
		?>

		<div class="row">
			<div class="col-sm-6">
				<?= $form->field($statisticsForm, 'customer') ?>
			</div>
			<div class="col-sm-6">
				<?= $form->field($statisticsForm, 'user')->dropDownList(ArrayHelper::map($users, 'username', 'username'), [
					'prompt' => ''
				]) ?>
			</div>
		</div>
		<div class="row">
			<?php /*<div class="col-sm-6">
				<?= $form->field($statisticsForm, 'userChanged') ?>
			</div>*/ ?>
			<div class="col-sm-6">
				<?= $form->field($statisticsForm, 'contractor')->dropDownList(ArrayHelper::map($contractors, 'id', 'companyName'), [
					'prompt' => ''
				]) ?>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="row">
					<div class="col-sm-8">
						<?php $form->fieldConfig['template'] = $leftDatePickerConfig; ?>
						<?= $form->field($statisticsForm, 'regDateFrom', ['inputOptions' => ['class' => 'form-control form-control_border-radius form-control_white-gradient js-datepicker']]) ?>
					</div>

					<div class="col-sm-4">
						<?php $form->fieldConfig['template'] = $rightDatePickerConfig; ?>
						<?= $form->field($statisticsForm, 'regDateTo', ['inputOptions' => ['class' => 'form-control form-control_border-radius form-control_white-gradient js-datepicker']]) ?>
					</div>
				</div>
				<?php $form->fieldConfig['template'] = '<div class="row"><div class="col-sm-4 nobr">{label}</div><div class="col-sm-8">{input}{error}</div></div>'; ?>
				<?= $form->field($statisticsForm, 'product') ?>
			</div>
			<div class="col-sm-6">
				<?= $form->field($statisticsForm, 'given')->radioList([
					'' => 'all',
					0  => 'not given',
					1  => 'given',
				]) ?>
			</div>
		</div>

		<div class="text-right">
			<?= Html::button('検索', ['class' => 'btn btn-primary button-blue', 'type' => 'submit']) ?>
		</div>
	</div>

	<div class="shadowed-box">
		<?= $this->render('@parts/statistic-table', [
			'statisticProducts' => $statisticProducts,
		]) ?>

		<div class="padded-box padded-box_no-margin-bottom light-grey-bg clearfix">
			<div class="row">
				<div class="col-sm-2">
					<a class="btn btn-primary button-blue button-blue_grey" href="<?= Url::to(['main/master']) ?>"><span
							class="icon-back"></span>戻る</a>
				</div>
				<div class="col-sm-4">
					<?= $form->field($statisticsForm, 'type')
						->dropDownList(['' => ''] + StatisticsForm::getTypesList(), [
							'class'                    => 'js-statistics-change-type form-control',
							'data-url'                 => Url::to(['ajax/statistic-change-type']),
							'data-customer'            => (string) $statisticsForm->customer,
							'data-contractor'          => (string) $statisticsForm->contractor,
							'data-get-customers-url'   => Url::to(['ajax/get-customers']),
							'data-get-contractors-url' => Url::to(['ajax/get-contractors']),
						]) ?>
				</div>
				<div class="col-sm-1">
					<label for="statisticsform-members" class="control-label">宛名</label>
				</div>
				<div class="col-sm-2">
					<div class="form-group">
						<select id="statisticsform-members" class="js-statistics-type-members form-control" name="StatisticsForm[members]">
						</select>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="pull-right">
						<?php if (count($statisticProducts)) { ?>
							<a class="btn btn-primary button-blue js-create-report-button" href="<?= Url::to(['main/statistics-create-report']) ?>">Create report</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php ActiveForm::end(); ?>