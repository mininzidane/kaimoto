<?php

use \yii\helpers\Html;
use \yii\helpers\Url;
use \yii\bootstrap\ActiveForm;
use \yii\helpers\ArrayHelper;

/**
 * @var  yii\web\View            $this
 * @var  \app\models\Product     $product
 * @var  \app\models\Category[]  $categories
 * @var \app\models\Contractor[] $contractors
 * @var int                      $currentPage
 **/

$addMode = isset($addMode)? $addMode: false;
?>

<div class="shadowed-box padded-box">
	<?php
	$form                          = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
	$form->fieldConfig['template'] = '<div class="row"><div class="col-sm-3">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-3">{error}</div></div>';
	?>

	<?= $form->field($product, 'title') ?>
	<?= $form->field($product, 'price') ?>
	<?= $form->field($product, 'purchasePrice') ?>

	<?php if ($product->picture) { ?>
		<div class="row form-group">
			<div class="col-sm-offset-3 col-sm-9">
				<?= Html::img($product->getImageUrl()); ?>
			</div>
		</div>
	<?php } ?>

	<?= $form->field($product, 'picture')->fileInput() ?>
	<?= $form->field($product, 'contractorId')->dropDownList(ArrayHelper::map($contractors, 'id', 'companyName')) ?>
	<?= $form->field($product, 'categoryId')
		->dropDownList(ArrayHelper::map($categories, 'id', 'title'), ['prompt' => '']) ?>
	<?= $form->field($product, 'code') ?>

	<div class="form-group row">
		<div class="col-xs-6">
			<a class="btn btn-primary button-blue button-blue_grey" href="<?= Url::to(array_merge(
					['main/products'],
					@$currentPage? ['page' => $currentPage, 'per-page' => Yii::$app->params['perPage']]: []
			)) ?>"><span class="icon-back"></span>戻る</a>
		</div>
		<div class="col-xs-6">
			<?= $addMode?
				Html::submitButton('<span class="glyphicon glyphicon-plus"></span>登録', ['class' => 'btn btn-primary button-blue pull-right']):
				Html::submitButton('<span class="glyphicon glyphicon-save-file"></span>更新', ['class' => 'btn btn-primary button-blue pull-right'])
			?>
		</div>
	</div>

	<?php ActiveForm::end(); ?>
</div>