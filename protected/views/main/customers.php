<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use yii\widgets\LinkPager;

/**
 * @var  yii\web\View           $this
 * @var  \app\models\Customer[] $customers
 * @var \yii\data\Pagination    $pages
 **/
?>

<div class="shadowed-box">
	<table class="table table-hover table-grey table-grey_no-border table-edit js-edit-table">
		<colgroup>
			<col width="5%">
			<col width="15%">
			<col width="20%">
			<col width="15%">
			<col width="15%">
			<col width="30%">
		</colgroup>
		<tr>
			<th>No.</th>
			<th>Name</th>
			<th>Address</th>
			<th>Number</th>
			<th>Created</th>
			<th></th>
		</tr>
		<?php foreach ($customers as $customer) { ?>
			<tr>
				<td><?= $customer->id ?></td>
				<td><?= $customer->name ?></td>
				<td><?= $customer->address ?></td>
				<td><?= $customer->number ?></td>
				<td><?= date(Yii::$app->params['dateFormat'], strtotime($customer->created)) ?></td>
				<td>
					<a href="<?= Url::to(['main/customers-edit', 'id' => $customer->id]) ?>"
					   class="table-edit__control table-edit__control_edit"><span
							class="table-edit__controls-label">編集</span></a>
					<button class="table-edit__control table-edit__control_delete" data-delete
							data-delete-url="<?= Url::to(['ajax/customers-delete', 'id' => $customer->id]) ?>">
						<span class="table-edit__controls-label">削除</span></button>
				</td>
			</tr>
		<?php } ?>
	</table>

	<div class="padded-box">
		<?= LinkPager::widget([
			'pagination' => $pages,
		]); ?>
	</div>

	<div class="padded-box padded-box_no-margin-bottom light-grey-bg">
		<div class="row">
			<div class="col-xs-6">
				<a class="btn btn-primary button-blue button-blue_grey" href="<?= Url::to(['main/master']) ?>"><span
						class="icon-back"></span>戻る</a>
			</div>
			<div class="col-xs-6">
				<a href="<?= Url::to(['main/customers-add']) ?>" class="pull-right btn btn-primary button-blue"
				   name="send"><span
						class="glyphicon glyphicon-plus"></span>登録</a>
			</div>
		</div>
	</div>
</div>

<?= $this->render('@parts/modal', [
	'text' => '注文を削除します。<br>よろしいですか？',
]) ?>