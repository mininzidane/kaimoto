<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use yii\widgets\LinkPager;

/**
 * @var  yii\web\View          $this
 * @var  \app\models\History[] $histories
 * @var \yii\data\Pagination   $pages
 **/
?>

<div class="shadowed-box">
	<form action="" method="post">
		<table class="table table-hover table-grey table-grey_no-border table-edit js-edit-table">
			<colgroup>
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="15%">
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="25%">
			</colgroup>
			<tr>
				<th>No.</th>
				<th><a href="<?= Url::to(Yii::$app->request->get('sortDirection')? ['main/history']: ['main/history', 'sortDirection' => 'asc']) ?>">日付</a></th>
				<th>更新日</th>
				<th>お客様</th>
				<th>担当者</th>
				<th>更新者</th>
				<th>届いた</th>
				<th></th>
			</tr>
			<?php foreach ($histories as $history) { ?>
				<tr class="<?= $history->sent? '': 'bold' ?>">
					<td><?= $history->getNumber() ?></td>
					<td><?= date(Yii::$app->params['dateFormat'], strtotime($history->created)) ?></td>
					<td><?= $history->modified? date(Yii::$app->params['dateFormat'], $history->modified): '' ?></td>
					<td><?= @$history->customer->name ?></td>
					<td><?= @$history->creator->username ?></td>
					<td><?= @$history->user->username ?></td>
					<td><?= $history->given? '✔': '' ?></td>
					<td>
						<div class="table-edit__controls">
							<?php if ($history->sent && !$history->given) { ?>
								<button class="table-edit__control" name="History[given]" value="<?= $history->id ?>"><span
										class="table-edit__controls-label">到達</span></button>
							<?php } ?>
							<?php if (!$history->sent) { ?>
								<a href="<?= Url::to(['main/history-edit', 'id' => $history->id]) ?>"
								   class="table-edit__control table-edit__control_edit"><span
										class="table-edit__controls-label">編集</span></a>
							<?php } ?>
							<button class="table-edit__control table-edit__control_delete" data-delete
									data-delete-url="<?= Url::to(['ajax/delete-history', 'id' => $history->id]) ?>">
								<span class="table-edit__controls-label">削除</span></button>
							<a href="<?= Url::to($history->sent? ['main/history-edit', 'id' => $history->id, 'sendNew' => true]: ['main/history-send', 'id' => $history->id]) ?>"
							   class="table-edit__control table-edit__control_next"><span
									class="table-edit__controls-label">再送</span></a>
						</div>
					</td>
				</tr>
			<?php } ?>
		</table>
	</form>

	<div class="padded-box">
		<?= LinkPager::widget([
			'pagination' => $pages,
		]); ?>
	</div>
</div>

<?= $this->render('@parts/modal', [
	'text' => '注文を削除します。<br>よろしいですか？',
]) ?>