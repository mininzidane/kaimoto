<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use \app\models\Product;

/**
 * @var  yii\web\View          $this
 * @var  \app\models\Product[] $products
 * @var array                  $cart
 **/
?>

<div class="shadowed-box padded-box">
	<div class="master-block">
		<div class="form-group row">
			<div class="col-sm-6">
				<a href="<?= Url::to(['main/products']) ?>" class="blue-link">
					<img src="/images/goods.png" alt="">
				</a>
			</div>
			<div class="col-sm-6">
				<a href="<?= Url::to(['main/users']) ?>" class="blue-link">
					<img src="/images/employee.png" alt="">
				</a>
			</div>
			<?php /*<div class="col-sm-4">
				<a href="<?= Url::to(['main/history']) ?>" class="blue-link">
					<img src="/images/cleaning.png" alt="">
				</a>
			</div>*/ ?>
		</div>

		<div class="form-group row">
			<div class="col-sm-4">
				<a href="<?= Url::to(['main/customers']) ?>" class="blue-link">
					<img src="/images/contractor.png" alt="">
				</a>
			</div>
			<div class="col-sm-4">
				<a href="<?= Url::to(['main/contractors']) ?>" class="blue-link">
					<img src="/images/customer.png" alt="">
				</a>
			</div>
			<div class="col-sm-4">
				<a href="<?= Url::to(['main/statistics']) ?>" class="blue-link">
					<img src="/images/statistics.png" alt="">
				</a>
			</div>
		</div>
	</div>
</div>