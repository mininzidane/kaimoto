<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use \app\models\User;

/**
 * @var  yii\web\View       $this
 * @var  \app\models\User[] $users
 **/
?>

<div class="shadowed-box">
	<table class="table table-hover table-grey table-grey_no-border table-edit js-edit-table">
		<colgroup>
			<col width="5%">
			<col width="25%">
			<col width="20%">
			<col width="20%">
			<col width="30%">
		</colgroup>
		<tr>
			<th>No.</th>
			<th>担当者</th>
			<th>パスワード</th>
			<th>権限</th>
			<th></th>
		</tr>

		<?php foreach ($users as $user) { ?>
			<tr data-read-row>
				<td><?= $user->id ?></td>
				<td><?= $user->username ?></td>
				<td>********</td>
				<td><?= $user->role ?></td>
				<td>
					<div class="table-edit__controls">
						<button class="table-edit__control table-edit__control_edit" data-edit><span class="table-edit__controls-label">編集</span></button>
						<?php if (count($users) > 1) { ?>
							<button class="table-edit__control table-edit__control_delete" data-delete data-delete-url="<?= Url::to(['ajax/delete-user', 'id' => $user->id]) ?>"><span class="table-edit__controls-label">削除</span></button>
						<?php } ?>
					</div>
				</td>
			</tr>
			<tr class="hidden" data-edit-row data-serialize-elements data-url="<?= Url::to(['ajax/change-user']) ?>">
				<td><?= $user->id ?></td>
				<td>
					<input type="hidden" name="id" value="<?= $user->id ?>">
					<input class="form-control" type="text" name="username" value="<?= $user->username ?>">
				</td>
				<td><input class="form-control" type="password" name="password" value=""></td>
				<td>
					<select class="form-control" name="role">
						<?php foreach (User::getRoles() as $roleLabel) {
							echo Html::tag('option', $roleLabel);
						} ?>
					</select>
				</td>
				<td>
					<div class="table-edit__controls">
						<button class="table-edit__control" data-accept><span class="table-edit__controls-label">保存</span></button>
						<button class="table-edit__control table-edit__control_cancel" data-cancel><span class="table-edit__controls-label">取消</span></button>
					</div>
				</td>
			</tr>
		<?php } ?>
	</table>

	<div class="padded-box padded-box_no-margin-bottom light-grey-bg clearfix">
		<a class="btn btn-primary button-blue button-blue_grey" href="<?= Url::to(['main/master']) ?>"><span
				class="icon-back"></span>戻る
		</a>
		<a href="<?= Url::to(['main/users-add']) ?>" class="pull-right btn btn-primary button-blue" name="send"><span class="glyphicon glyphicon-plus"></span>登録</a>
	</div>
</div>

<?= $this->render('@parts/modal', [
	'text' => '削除してもよろしですか？',
]) ?>