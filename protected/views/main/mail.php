<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use \yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\components\CustomerSelectWidget;

/**
 * @var  yii\web\View            $this
 * @var  \app\models\Product[]   $products
 * @var array                    $cart
 * @var array                    $productGroups
 * @var string                   $params
 * @var \app\models\History      $history
 * @var \app\models\Contractor[] $companies
 **/

/** @var \app\models\User $user */
$user = Yii::$app->user->getIdentity();
$comments = Yii::$app->request->post('comment', []);
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'padded-box padded-box_no-margin-bottom light-grey-bg clearfix', 'method' => 'post']]); ?>

	<?php foreach ($productGroups as $company => $products) { ?>
		<input type="hidden" name="company[]" value="<?= $company ?>">

		<div class="shadowed-box padded-box">
			<p>送信者： <?= $user->senderEmail ?></p>

			<p>受信者： <?= @$companies[$company]->email ?> [<?= @$companies[$company]->companyName ?>]</p>

			<p>注文：</p>

			<?= $this->render('@parts/product-table', [
				'products' => $products,
				'cart'     => $cart,
			]) ?>

			<p>備考：</p>

			<div class="form-group">
				<textarea name="comment[]" class="form-control"><?php echo current($comments); next($comments) ?></textarea>
			</div>
			<div class="row">
				<div class="col-xs-3">
					<button class="btn btn-primary button-blue button-blue_grey" type="submit"
							name="clear" value="<?= $company ?>"><span class="glyphicon glyphicon-remove"></span>破棄
					</button>
				</div>
			</div>
		</div>
	<?php } ?>

	<div class="padded-box padded-box_no-margin-bottom light-grey-bg clearfix">
		<?= CustomerSelectWidget::widget([
				'form'    => $form,
				'history' => $history,
		]) ?>

		<a class="btn btn-primary button-blue button-blue_grey" href="<?= Url::to(['catalog']) ?>"><span
				class="icon-back"></span>戻る
		</a>

		<button class="btn btn-primary button-blue pull-right" type="submit" <?= $history->customerId? '': 'disabled' ?>
				name="send"><span class="glyphicon glyphicon-envelope"></span>送信
		</button>
		<?php /*<a href='<?= $history->customerId? Url::to(['ajax/get-json']) . '?params=' . $params: 'javascript: void(0)' ?>'
		   target="_blank"
		   <?= $history->customerId? '': 'style="opacity: 0.2"' ?>
		   class="btn btn-primary button-blue pull-right"><span class="glyphicon glyphicon-print"></span>プリントアウト</a> */ ?>
	</div>

<?php ActiveForm::end(); ?>