<?php

use \yii\helpers\Html;
use \yii\helpers\Url;
use \yii\helpers\ArrayHelper;
use \yii\bootstrap\ActiveForm;

/**
 * @var  yii\web\View           $this
 * @var  \app\models\Contractor $contractor
 **/

$addMode = isset($addMode)? $addMode: false;
?>

<div class="shadowed-box padded-box">
	<?php
	$form                          = ActiveForm::begin();
	$form->fieldConfig['template'] = '<div class="row"><div class="col-sm-3">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-3">{error}</div></div>';
	?>

	<?= $form->field($contractor, 'companyName') ?>
	<?= $form->field($contractor, 'email') ?>

	<div class="form-group row">
		<div class="col-xs-6">
			<a class="btn btn-primary button-blue button-blue_grey" href="<?= Url::to(['main/contractors']) ?>"><span
					class="icon-back"></span>戻る</a>
		</div>
		<div class="col-xs-6">
			<?= $addMode?
				Html::submitButton('<span class="glyphicon glyphicon-plus"></span>登録', ['class' => 'btn btn-primary button-blue pull-right']):
				Html::submitButton('<span class="glyphicon glyphicon-save-file"></span>更新', ['class' => 'btn btn-primary button-blue pull-right'])
			?>
		</div>
	</div>

	<?php ActiveForm::end(); ?>
</div>