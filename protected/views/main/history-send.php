<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\components\CustomerSelectWidget;

/**
 * @var  yii\web\View          $this
 * @var  \app\models\Product[] $products
 * @var array                  $cart
 * @var array                  $productGroups
 * @var string                 $params
 * @var \app\models\History    $history
 **/

$this->registerJsFile('js/history.js', ['depends' => ['yii\web\JqueryAsset']]);
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'clearfix', 'method' => 'post']]); ?>

	<?php foreach ($productGroups as $company => $products) { ?>
		<input type="hidden" name="company[]" value="<?= $company ?>">

		<div class="shadowed-box padded-box">
			<p>注文：</p>

			<?= $this->render('@parts/product-table', [
				'products' => $products,
				'cart'     => $cart,
			]) ?>
		</div>
	<?php } ?>

	<div class="padded-box padded-box_no-margin-bottom light-grey-bg clearfix">
		<?= CustomerSelectWidget::widget([
			'form'                 => $form,
			'history'              => $history,
			'withoutPageReloading' => true,
		]) ?>

		<button type="submit" class="btn btn-primary button-blue pull-right"
				name="send"><span class="glyphicon glyphicon-envelope"></span>送信
		</button>
		<a href='<?= Url::to(['ajax/get-json']) . '?params=' . $params ?>'
		   target="_blank"
		   class="btn btn-primary button-blue pull-right"><span class="glyphicon glyphicon-print"></span>プリントアウト</a>
		<a class="btn btn-primary button-blue button-blue_grey" href="<?= Url::to(['main/history']) ?>"><span
				class="icon-back"></span>戻る
		</a>
	</div>

<?php ActiveForm::end(); ?>