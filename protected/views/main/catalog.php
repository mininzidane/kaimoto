<?php

use yii\helpers\Html;
use \yii\helpers\Url;

/**
 * @var $this       yii\web\View
 * @var $categories \app\models\Category[]
 **/

$this->registerJsFile('/js/cart.js', ['depends' => ['yii\web\JqueryAsset']]);
?>

<ul class="nav nav-tabs green-tabs">
	<?php foreach ($categories as $i => $category) {
		echo Html::tag('li', Html::a($category->title, "#category{$i}", ['data-toggle' => 'tab']), $i == 0? ['class' => 'active']: []);
	} ?>
</ul>

<div class="tab-content green-tabs__content js-products">
	<?php foreach ($categories as $i => $category) { ?>
		<div class="tab-pane fade<?= $i == 0? ' active in': '' ?>" id="<?= "category{$i}" ?>">
			<div class="row">
				<?php foreach ($category->products as $product) { ?>
					<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
						<div class="product js-product<?= $product->checkIsInCart()? ' product_checked': '' ?>"
							 data-product-id="<?= $product->id ?>"
							 data-add-to-cart-url="<?= Url::to(['ajax/add-to-cart']) ?>"
							 data-remove-from-cart-url="<?= Url::to(['ajax/remove-from-cart']) ?>"
							>
							<?= Html::img($product->imageUrl) ?>
							<div class="product__info">
								<div><?= $product->title ?></div>
								<div class="product__company"><?= @$product->company->companyName ?></div>
								<div class="product__price">
									¥<?= Yii::$app->formatter->asDecimal($product->price, 0) ?>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	<?php } ?>
</div>