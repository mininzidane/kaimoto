<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use yii\widgets\LinkPager;

/**
 * @var  yii\web\View          $this
 * @var  \app\models\Product[] $products
 * @var \yii\data\Pagination   $pages
 **/
?>

	<div class="shadowed-box">
		<table class="table table-hover table-grey table-grey_no-border table-edit js-edit-table">
			<colgroup>
				<col width="5%">
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="25%">
			</colgroup>
			<tr>
				<th>No.</th>
				<th>画像</th>
				<th>商品名</th>
				<th>値段 <span style="font-weight: normal">(税込み)</span></th>
				<th>仕入れ値</th>
				<th>供給者</th>
				<th>カテゴリー</th>
				<th>商品コード</th>
				<th></th>
			</tr>
			<?php foreach ($products as $product) { ?>
				<tr>
					<td><?= $product->id ?></td>
					<td>
						<?php if ($product->picture) {
							echo Html::img($product->getImageUrl());
						} ?>
					</td>
					<td><?= $product->title ?></td>
					<td><?= $product->price ?>円</td>
					<td><?= $product->purchasePrice?: 0 ?>円</td>
					<td><?= @$product->company->companyName ?></td>
					<td><?= $product->category->title ?></td>
					<td><?= $product->code ?></td>
					<td>
						<div class="table-edit__controls">
							<a href="<?= Url::to(['main/products-edit', 'id' => $product->id]) ?>"
							   class="table-edit__control table-edit__control_edit"><span
									class="table-edit__controls-label">編集</span></a>
							<button class="table-edit__control table-edit__control_delete" data-delete
									data-delete-url="<?= Url::to(['ajax/delete-product', 'id' => $product->id]) ?>">
								<span class="table-edit__controls-label">削除</span></button>
						</div>
					</td>
				</tr>
			<?php } ?>
		</table>

		<div class="padded-box">
			<?= LinkPager::widget([
				'pagination' => $pages,
			]); ?>
		</div>

		<div class="padded-box padded-box_no-margin-bottom light-grey-bg clearfix">
			<a href="<?= Url::to(['main/products-add']) ?>" class="pull-right btn btn-primary button-blue" name="send"><span class="glyphicon glyphicon-plus"></span>登録</a>
		</div>
	</div>

<?= $this->render('@parts/modal', [
	'text' => '注文を削除します。<br>よろしいですか？',
]) ?>