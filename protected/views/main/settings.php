<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use \app\models\Product;

/**
 * @var  yii\web\View          $this
 * @var  \app\models\Product[] $products
 * @var array                  $cart
 **/
?>

<div class="shadowed-box padded-box">
	<button class="btn btn-primary button-blue button-blue_grey"
			name="clear"><span class="glyphicon glyphicon-remove"></span>破棄
	</button>
	<button class="btn btn-primary button-blue pull-right"
			name="send"><span class="glyphicon glyphicon-save-file"></span>保存
	</button>
</div>