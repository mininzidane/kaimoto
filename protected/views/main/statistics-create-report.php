<?php
use \yii\helpers\Html;
use \yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/**
 * @var  yii\web\View $this
 * @var array         $statisticProducts
 */

$hideButtons = isset($hideButtons)? $hideButtons: false;
?>

<style>
	body {
		font-family: 'Sun-ExtA', 'Helvetica Neue', Helvetica, Arial, sans-serif;
	}
</style>

<?= $this->render('@parts/statistic-top-text', [
	'statisticProducts' => $statisticProducts,
]) ?>
<div class="shadowed-box">
	<?= $this->render('@parts/statistic-table-report', [
		'statisticProducts' => $statisticProducts,
	]) ?>

	<?php if (!$hideButtons) { ?>
		<div class="padded-box padded-box_no-margin-bottom light-grey-bg clearfix hide-from-print">
			<div class="pull-right">
				<a href="#" onclick="window.print(); return false;" target="_blank"
				   class="btn btn-primary button-blue"><span class="glyphicon glyphicon-print"></span>プリントアウト</a>
				<a class="btn btn-primary button-blue button-blue_grey"
				   href="<?= Url::to(['main/save-statistic-report']) ?>">Save to PDF</a>
			</div>
			<a class="btn btn-primary button-blue button-blue_grey" href="<?= Url::to(['main/statistics']) ?>"><span
					class="icon-back"></span>戻る</a>
		</div>
	<?php } ?>
</div>
