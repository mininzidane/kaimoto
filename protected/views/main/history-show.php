<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use \app\models\Product;

/**
 * @var  yii\web\View          $this
 * @var  \app\models\Product[] $products
 * @var array                  $cart
 * @var array                  $productGroups
 * @var string                 $params
 **/
?>

<?php foreach ($productGroups as $company => $products) { ?>
	<div class="shadowed-box padded-box">
		<p>注文：</p>

		<?= $this->render('@parts/product-table', [
			'products' => $products,
			'cart'     => $cart,
		]) ?>
	</div>
<?php } ?>

<div class="clearfix">
	<a href='<?= Url::to(['ajax/get-json']) . '?params=' . $params ?>' target="_blank"
	   class="btn btn-primary button-blue pull-right"><span class="glyphicon glyphicon-print"></span>プリントアウト</a>
</div>
