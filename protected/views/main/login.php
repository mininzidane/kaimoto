<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \yii\helpers\ArrayHelper;

/**
 * @var yii\web\View             $this
 * @var yii\bootstrap\ActiveForm $form
 * @var app\models\LoginForm     $model
 * @var \app\models\User[]       $users
 */

$this->title = 'Login';
?>
<div class="row">
	<div class="col-sm-6 col-sm-offset-3">
		<div class="login-form shadowed-box">
			<?php $form = ActiveForm::begin([
				'id'          => 'login-form',
				'options'     => ['class' => 'form-horizontal'],
				'fieldConfig' => [
					'template'     => "<div class=\"\">{input}</div>\n<div class=\"\">{error}</div>",
					'labelOptions' => ['class' => 'col-lg-1 control-label'],
				],
			]); ?>

			<?= $form->field($model, 'username')->dropDownList(array_merge(['' => '担当者選択'], ArrayHelper::map($users, 'username', 'username'))) ?>

			<?= $form->field($model, 'password')->passwordInput(['placeholder' => 'パスワード']) ?>

			<?php /*$form->field($model, 'rememberMe', [
		'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
	])->checkbox()*/ ?>

			<div class="form-group t-c">
				<?= Html::submitButton('ログイン', ['class' => 'btn btn-primary button-blue login-button', 'name' => 'login-button']) ?>
			</div>

			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>