<?php
use app\models\Product;
use yii\helpers\Url;

$cartCount = count(Product::getCart());
?>
<?php if ($cartCount) { ?>
	<div class="cart-cloud">
		<a href="<?= Url::to(['main/mail']) ?>"><span class="cart-cloud__list-icon"><span
					class="cart-cloud__cloud"><?= $cartCount ?></span></span></a>
	</div>
<?php } ?>