<?php
/**
 * @var \app\models\Product[] $products
 * @var array                 $cart
 * @var bool                  $editMode
 * @var String                $comment
 */

use \yii\helpers\Url;

$comment = isset($comment)? $comment: null;
?>

<table class="table table-grey">
	<tr>
		<th>商品名</th>
		<th>商品コード</th>
		<th>仕入れ値</th>
		<th>数量</th>
		<th>計</th>
	</tr>

	<?php $sum = 0; ?>
	<?php foreach ($products as $product) { ?>
		<tr data-product-id="<?= $product->id ?>">
			<td><?= $product->title ?></td>
			<td><?= $product->code ?></td>
			<td><?= $product->purchasePrice ?></td>
			<td><?= $cart[$product->id]; ?></td>
			<td><?= ($rowTotalPrice = $product->purchasePrice * $cart[$product->id]) ?></td>
		</tr>
		<?php $sum += $rowTotalPrice ?>
	<?php } ?>
	<tr class="table-grey__footer">
		<td style="text-align: left">
			<?php if ($comment !== null) { ?>
				備考: <?= $comment ?>
			<?php } ?>
		</td>
		<td colspan="2">
			小計 <br>
			消費税（8%）<br>
			合計金額
		</td>
		<td>
			¥<?= Yii::$app->formatter->asDecimal($sum, 0) ?><br>
			¥<?= Yii::$app->formatter->asDecimal($sum * 0.08, 0) ?><br>
			¥<?= Yii::$app->formatter->asDecimal($sum * 1.08, 0) ?><br>
		</td>
	</tr>
</table>