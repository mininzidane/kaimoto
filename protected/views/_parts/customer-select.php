<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * @var \yii\bootstrap\ActiveForm $form
 * @var \app\models\History       $history
 * @var \app\models\Customer[]    $customers
 * @var bool                      $withoutPageReloading
 * @var array                     $options
 */

$form->fieldConfig['template'] = '<div class="row"><div class="col-sm-3">{label}</div><div class="col-sm-9">{input}{error}</div></div>';
?>

<?php // For future best times))) When guys realize that they have too much customers
/*= $form->field($history, 'customerId')
	->dropDownList($history->customerId? [$history->customerId => $history->customer->name]: [], ['class' => 'js-select-picker js-picker-get-options-by-query js-submit-on-change', 'data-url' => Url::to(['ajax/get-customers']), 'prompt' => 'お客様選択'])*/ ?>
<?= $form->field($history, 'customerId')->dropDownList(ArrayHelper::map($customers, 'id', 'name'), $options + [
	'class'  => ($withoutPageReloading? '': 'js-submit-on-change ') . 'form-control',
	'prompt' => 'お客様選択',
]) ?>

<div class="text-right form-group">
	<a href="<?= Url::to(['main/customers-add']) ?>" class="btn btn-primary button-blue button-blue_grey"><span
			class="glyphicon glyphicon-plus"></span>登録</a>
</div>