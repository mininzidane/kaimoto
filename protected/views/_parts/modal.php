<?php
/**
 * @var string $text
 * @var string $noText
 * @var string $yesText
 */

$yesText = isset($yesText)? $yesText: 'はい';
$noText = isset($noText)? $noText: 'いいえ';
?>

<div class="modal fade modal-red" id="confirmation-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<?= $text ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-right js-modal-yes"><?= $yesText ?></button>
				<button type="button" class="btn btn-success js-modal-no"><?= $noText ?></button>
			</div>
		</div>
	</div>
</div>