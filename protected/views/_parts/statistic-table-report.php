<?php
/**
 * @var array $statisticProducts
 */
?>

<?php
use \app\models\StatisticsForm;

$totalPrice = 0;
$colSpan    = 12;

if (!count($statisticProducts)) {
	return;
}
?>
<table class="table table-hover table-grey table-grey_no-border table-edit js-edit-table">
	<tr>
		<th>商品名</th>
		<th>数量</th>
		<th><?= current($statisticProducts)['type'] == StatisticsForm::TYPE_CUSTOMER? '単価': '仕入れ値' ?></th>
		<th>金額</th>
	</tr>
	<?php foreach ($statisticProducts as $productItem) { ?>
		<tr data-id="<?= $productItem['id'] ?>">
			<td><?= $productItem['title'] ?></td>
			<td><?= $productItem['quantity'] ?></td>
			<td><?= $productItem['price'] ?></td>
			<td><?= $productItem['sum'] ?></td>
		</tr>
		<?php $totalPrice += $productItem['quantity'] * $productItem['price'] ?>
	<?php } ?>
	<tr>
		<td colspan="<?= $colSpan ?>" class="text-right">
			小計: ¥<?= Yii::$app->formatter->asDecimal($totalPrice, 0) ?><br>
			消費税（8%): ¥<?= Yii::$app->formatter->asDecimal($totalPrice * 0.08, 0) ?><br>
			合計金額: ¥<?= Yii::$app->formatter->asDecimal($totalPrice * 1.08, 0) ?><br>
		</td>
	</tr>
</table>