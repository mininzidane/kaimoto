<?php
/**
 * @var  yii\web\View $this
 * @var array         $statisticProducts
 */

// calculate total sum
$total = 0;
foreach ($statisticProducts as $statisticProduct) {
	$total += $statisticProduct['sum'];
}
?>
<div class="row form-group">
	<div class="col-sm-6">
		<h1><?= $statisticProduct['pageTitle'] ?></h1>
		<p><?= $statisticProduct['pageName'] ?></p>
		<p><?= $statisticProduct['member'] ?></p>
	</div>
	<div class="col-sm-6">
		<div class="text-right">
			発行日：<?= date('Y年m月d日', time()) ?> <br>
		</div>
	</div>
</div>
<div class="row form-group">
	<div class="col-sm-6">
		<?php /*<p><?= $contractor->companyName ?> 様</p>*/ ?>
		<p>下記の通り発注いたします。</p>
		<p>発注金額&nbsp;&nbsp;&nbsp;<strong style="font-size: 18px">¥<?= Yii::$app->formatter->asDecimal($total, 0) ?></strong>
		</p>
	</div>
	<div class="col-sm-6">
		<div class="text-right">
			<?= Yii::$app->params['addressForStatistics'] ?>
		</div>
	</div>
</div>