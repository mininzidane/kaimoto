<?php
/**
 * @var array $statisticProducts
 */
?>

<?php
$totalPrice = 0;
$colSpan    = 12;

if (!count($statisticProducts)) {
	return;
}
?>
<table class="table table-hover table-grey table-grey_no-border table-edit js-edit-table">
	<colgroup>
		<col width="11%">
		<col width="9%">
		<col width="10%">
		<col width="10%">
		<col width="10%">
		<col width="9%">
		<col width="6%">
		<col width="6%">
		<col width="8%">
		<col width="5%">
		<col width="8%">
		<col width="8%">
	</colgroup>
	<tr>
		<th>注文番号</th>
		<th>枝番号</th>
		<th>利用者</th>
		<th>登録者</th>
		<th>商品名</th>
		<th>数量</th>
		<th>単価</th>
		<th>仕入れ値</th>
		<th>金額</th>
		<th>届いた</th>
		<th>仕入先</th>
		<th>登録日</th>
	</tr>
	<?php foreach ($statisticProducts as $productItem) { ?>
		<tr data-id="<?= $productItem['id'] ?>">
			<td><?= $productItem['number'] ?></td>
			<td><?= $productItem['index'] ?></td>
			<td><?= $productItem['customer'] ?></td>
			<td><?= $productItem['user'] ?></td>
			<td><?= $productItem['title'] ?></td>
			<td><?= $productItem['quantity'] ?></td>
			<td><?= $productItem['price'] ?></td>
			<td><?= $productItem['purchasePrice'] ?></td>
			<td><?= $productItem['sum'] ?></td>
			<td><?= $productItem['given']? '✔': '' ?></td>
			<td><?= $productItem['company'] ?></td>
			<td><?= date(Yii::$app->params['dateFormat'], strtotime($productItem['created'])) ?></td>
		</tr>
		<?php $totalPrice += $productItem['quantity'] * $productItem['price'] ?>
	<?php } ?>
	<tr>
		<td colspan="<?= $colSpan ?>" class="text-right">
			Total: ¥<?= Yii::$app->formatter->asDecimal($totalPrice, 0) ?>
		</td>
	</tr>
</table>