<?php
/**
 * @var \app\models\Product[] $products
 * @var array                 $cart
 * @var bool $editMode
 */

use \yii\helpers\Url;

$editMode = isset($editMode) && $editMode?: false;
?>

<table class="table table-grey">
	<tr>
		<?php if ($editMode) { ?>
			<th>
				<label>
					<input type="checkbox" class="js-cart-select-all">
				</label>
			</th>
		<?php } ?>
		<th></th>
		<th>商品名</th>
		<th>商品コード</th>
		<th>単価</th>
		<th>仕入れ値</th>
		<th>数量</th>
		<th>計</th>
	</tr>

	<?php $sum = 0; ?>
	<?php foreach ($products as $i => $product) { ?>
		<tr data-product-id="<?= $product->id ?>">
			<?php if ($editMode) { ?>
				<td>
					<label>
						<input type="checkbox" class="js-cart-select" name="deleteProducts[]" value="<?= $product->id ?>">
					</label>
				</td>
			<?php } ?>
			<td><?= ++$i ?>.</td>
			<td><?= $product->title ?></td>
			<td class="muted"><?= $product->code ?></td>
			<td><?= $product->price ?></td>
			<td><?= $product->purchasePrice ?></td>
			<td>
				<?php if ($editMode) { ?>
					<input type="text" class="form-control table-checkbox" name="quantityProducts[<?= $product->id ?>]"
						   value="<?= $cart[$product->id] ?>" data-quantity="<?= $cart[$product->id] ?>" data-url="<?= Url::to(['ajax/history-change-quantity']) ?>">
				<?php } else {
					echo $cart[$product->id];
				} ?>
			</td>
			<td><?= ($rowTotalPrice = $product->price * $cart[$product->id]) ?></td>
		</tr>
		<?php $sum += $rowTotalPrice ?>
	<?php } ?>
	<tr class="table-grey__footer">
		<td colspan="<?= $editMode? 8: 7 ?>">
			合計： <strong class="red f18">¥<?= Yii::$app->formatter->asDecimal($sum, 0) ?></strong><br>
			税込み
		</td>
	</tr>
</table>