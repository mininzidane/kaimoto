<?php

use yii\helpers\Html;
use yii\i18n\Formatter;
use yii\helpers\Url;

/**
 * @var \app\models\Product[] $products
 * @var array                 $cart
 * @var int                   $sum
 */
?>
<div class="cart-summary-wrapper js-cart-summary">
	<div class="container">
		<div class="cart-summary__icon js-cart-toggle"><span class="glyphicon glyphicon-shopping-cart"></span></div>
	</div>
	<div class="cart-summary">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="cart-summary__table-wrapper">
						<table class="table cart-summary__table">
							<tr>
								<th></th>
								<th>
									<label>
										<input type="checkbox" class="js-cart-select-all">
										&nbsp;&nbsp;&nbsp;商品名
									</label>
								</th>
								<th>単価</th>
								<th>数量</th>
								<th>計</th>
							</tr>
							<?php $i = 1 ?>
							<?php foreach ($products as $product) { ?>
								<tr data-product-id="<?= $product->id ?>">
									<td><?= $i++ ?>.</td>
									<td>
										<label>
											<input type="checkbox" class="js-cart-select">
											&nbsp;&nbsp;&nbsp;<?= $product->title ?></label>
									</td>
									<td><?= $product->price ?></td>
									<td>
										<input type="number" class="form-control js-change-quantity"
											   value="<?= $cart[$product->id] ?>"
											   data-quantity="<?= $cart[$product->id] ?>"
											   data-url="<?= Url::to(['ajax/change-quantity']) ?>">
									</td>
									<td><?= $cart[$product->id] * $product->price ?></td>
								</tr>
							<?php } ?>
						</table>
					</div>
				</div>
				<div class="col-sm-3">
					<a class="white-border-block t-c cart-summary__link <?= count($cart)? '': 'js-empty-cart' ?>" href="<?= Url::to(['main/mail']) ?>">
						<div>合計：</div>
						<div class="cart-summary__sum">¥<?= Yii::$app->formatter->asDecimal($sum, 0) ?></div>
						<div>注文送信&nbsp;&nbsp;&nbsp;<span
									class="glyphicon glyphicon-arrow-right"></span></div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-9">
					<button class="btn cart-summary__button js-remove-products"
							data-url="<?= Url::to(['ajax/remove-from-cart']) ?>"><span
							class="glyphicon glyphicon-trash"></span>&nbsp;&nbsp;&nbsp;明細削除
					</button>
				</div>
				<div class="col-sm-3">
					<?php /*<button class="js-cart-save-history btn cart-summary__button cart-summary__button_w100"
							data-url="<?= Url::to(['ajax/save-history']) ?>">注文保存&nbsp;&nbsp;&nbsp;<span
							class="glyphicon glyphicon-save-file"></span></button>*/ ?>
					<a class="btn cart-summary__button cart-summary__button_w100 <?= count($cart)? '': 'js-empty-cart' ?>" href="<?= Url::to(['ajax/save-history']) ?>">
						注文保存&nbsp;&nbsp;&nbsp;<span
							class="glyphicon glyphicon-save-file"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
