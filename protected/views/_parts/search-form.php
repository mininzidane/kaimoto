<form action="" class="search-form">
	<div class="input-group">
		<input class="form-control" type="text" name="q" value="<?= Yii::$app->request->get('q') ?>">
		<div class="input-group-btn">
			<button class="btn btn-default"><span class="glyphicon glyphicon-search"></span> 検索</button>
		</div>
	</div>
</form>