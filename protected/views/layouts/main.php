<?php
use yii\helpers\Html;

use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use app\assets\AppAsset;
use app\models\Product;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$action = Yii::$app->controller->action->id;
$this->registerCssFile('/css/print.css', ['media' => 'print']);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
<?php if (Yii::$app->params['showTopMenu'] && !Yii::$app->user->isGuest) { ?>
	<div class="top-menu">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 nobr">
					<button class="navbar-toggle js-menu-toggle" type="button">
						<span class="sr-only"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<span class="top-menu__text"><?= $this->title ?></span>
				</div>

				<div class="col-sm-8">
					<?php
					// show search form on specified actions
					if (in_array($action, ['users', 'products', 'history', 'master-history', 'contractors', 'customers'])) {
						echo $this->render('@parts/search-form');
					}
					?>
				</div>

				<div class="col-sm-1">
					<div class="js-cart-counter pull-right" data-url="<?= Url::to(['ajax/get-counter']) ?>">
						<?= $this->render('@parts/cart-counter') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<nav class="js-nav-bar nav-bar">
		<button class="navbar-toggle js-menu-toggle" type="button">
			<span class="sr-only"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<div class="nav-bar__title"><?= Yii::$app->user->getIdentity()->username ?></div>

		<?php echo Nav::widget([
			'options' => ['class' => 'nav-bar__list'],
			'items'   => [
				[
					'label'   => '注文',
					'url'     => ['main/catalog'],
					'options' => ['class' => 'nav-bar__catalog'],
				],
				[
					'label'   => '履歴',
					'url'     => ['main/history'],
					'options' => ['class' => 'nav-bar__history'],
				],
				[
					'label'   => 'マスタ',
					'url'     => ['main/master'],
					'options' => ['class' => 'nav-bar__master'],
					'visible' => Yii::$app->user->can('adminActions'),
				],
				[
					'label'   => '設定',
					'url'     => ['main/settings'],
					'options' => ['class' => 'nav-bar__settings'],
					'visible' => false,
				],
				[
					'label'   => 'ログアウト',
					'url'     => ['main/logout'],
					'options' => ['class' => 'nav-bar__logout'],
				],
			],
		]); ?>
	</nav>
<?php } ?>
<div class="content">
	<div class="container">
		<?= $content ?>
	</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
