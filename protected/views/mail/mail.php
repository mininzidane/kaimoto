<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use \app\models\Product;

/**
 * @var  yii\web\View          $this
 * @var  \app\models\Product[] $products
 * @var array                  $cart
 * @var string                 $comment
 * @var \app\models\Contractor $contractor
 * @var \app\models\History    $history
 **/
?>

<style>
	body {
		font-family: 'Sun-ExtA', 'Helvetica Neue', Helvetica, Arial, sans-serif;
	}
	.top-menu {
		display: none;
	}
</style>
<div class="row form-group">
	<div class="col-sm-6">
		<h1>注文書</h1>
	</div>
	<div class="col-sm-6">
		<div class="text-right">
			発行日：<?= date('Y年m月d日', $history->created? strtotime($history->created): time()) ?> <br>
			注文番号：<?= $history->getNumber() ?>
		</div>
	</div>
</div>
<div class="row form-group">
	<div class="col-sm-6">
		<p>株式会社<?= $contractor->companyName ?> 様</p>
		<p>下記の通り発注いたします。</p>
		<p>発注金額&nbsp;&nbsp;&nbsp;<strong style="font-size: 18px">¥<?= Yii::$app->formatter->asDecimal(Product::getCartSum(), 0) ?></strong></p>
	</div>
	<div class="col-sm-6">
		<div class="text-right">
			<?= Yii::$app->params['addressForStatistics'] ?>
		</div>
	</div>
</div>
<div class="shadowed-box padded-box">
	<?= $this->render('@parts/product-table-report', [
		'products' => $products,
		'cart'     => $cart,
		'comment'  => $comment,
	]) ?>
</div>