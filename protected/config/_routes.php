<?php

return [
	'/'                           => 'main/index',
	'/users/add/'                 => 'main/users-add',
	'/history/<id:\d+>/'          => 'main/history-show',
	'/history/edit/<id:\d+>/'     => 'main/history-edit',
	'/history/send/<id:\d+>/'     => 'main/history-send',
	'/products/edit/<id:\d+>/'    => 'main/products-edit',
	'/contractors/edit/<id:\d+>/' => 'main/contractors-edit',
	'/ajax/get-cart-json/'        => 'ajax/get-json', // for compatibility with old rule

	'/<action>/'      => 'main/<action>',
	'/ajax/<action>/' => 'ajax/<action>',
];
